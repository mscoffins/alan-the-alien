#pragma once
#include "glew.h"
#include "GameObject.h"

class Platform : public GameObject{
protected:
	GLuint texture;
public:
	Platform();
	Platform(float w, float h, Point2d, GLuint tex);
	void draw();;
};