#include "EndScreen.h"
//default constructor
EndScreen::EndScreen(){
	exit = false;
	active = false;
	resume = false;
	texNames.clear();
	numTexNames.push_back("HUD/hud_0.png");
	numTexNames.push_back("HUD/hud_1.png");
	numTexNames.push_back("HUD/hud_2.png");
	numTexNames.push_back("HUD/hud_3.png");
	numTexNames.push_back("HUD/hud_4.png");
	numTexNames.push_back("HUD/hud_5.png");
	numTexNames.push_back("HUD/hud_6.png");
	numTexNames.push_back("HUD/hud_7.png");
	numTexNames.push_back("HUD/hud_8.png");
	numTexNames.push_back("HUD/hud_9.png");
	numTexNames.push_back("HUD/colon.png");
	texNames.push_back("Menu/resumeButton.png");
	texNames.push_back("Menu/resumeButton2.png");
	texNames.push_back("Menu/exitButton.png");
	texNames.push_back("Menu/exitButton2.png");
	texNames.push_back("bg_castle.png");
	resumeButton = GameObject(10,2.5,Point2d(0,-5));
	exitButton = GameObject(10,2.5,Point2d(0,-10));
}
//constructor
EndScreen::EndScreen(int levelTime, int parTime, Point2d* mousePos, bool* leftPressed){
	this->mousePos = mousePos;
	this->leftPressed = leftPressed;
	this->levelTime = levelTime;
	score = 10*(parTime - levelTime);
	texNames.clear();
	numTexNames.push_back("HUD/hud_0.png");
	numTexNames.push_back("HUD/hud_1.png");
	numTexNames.push_back("HUD/hud_2.png");
	numTexNames.push_back("HUD/hud_3.png");
	numTexNames.push_back("HUD/hud_4.png");
	numTexNames.push_back("HUD/hud_5.png");
	numTexNames.push_back("HUD/hud_6.png");
	numTexNames.push_back("HUD/hud_7.png");
	numTexNames.push_back("HUD/hud_8.png");
	numTexNames.push_back("HUD/hud_9.png");
	numTexNames.push_back("HUD/colon.png");
	texNames.push_back("Menu/resumeButton.png");
	texNames.push_back("Menu/resumeButton2.png");
	texNames.push_back("Menu/exitButton.png");
	texNames.push_back("Menu/exitButton2.png");
	texNames.push_back("bg_castle.png");
	exit = false;
	active = false;
	resume = false;
	resumeButton = GameObject(10,2.5,Point2d(0,0));
	exitButton = GameObject(10,2.5,Point2d(0,-5));
}
//draw method
void EndScreen::draw(){
	//draw background
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	
	glBindTexture(GL_TEXTURE_2D, textures.at(4));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 4.0);glVertex2f(-40,40);
		glTexCoord2f(0.0, 0.0);glVertex2f(-40,-40);
		glTexCoord2f(4.0, 0.0);glVertex2f(40,-40);
		glTexCoord2f(4.0, 4.0);glVertex2f(40,40);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw time
	int tempTime = 0;
	int mins = levelTime/60;
	int seconds = levelTime%60;
	//10 mins
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at((int)mins/10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(-4 ,13);
		glTexCoord2f(0.0, 0.0);glVertex2f(-4 , 11);
		glTexCoord2f(1.0, 0.0);glVertex2f(-2, 11);
		glTexCoord2f(1.0, 1.0);glVertex2f(-2, 13);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//mins
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at((int)mins%10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(-2 ,13);
		glTexCoord2f(0.0, 0.0);glVertex2f(-2 , 11);
		glTexCoord2f(1.0, 0.0);glVertex2f(0, 11);
		glTexCoord2f(1.0, 1.0);glVertex2f(0, 13);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//colon
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at(10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(0 ,13);
		glTexCoord2f(0.0, 0.0);glVertex2f(0 , 11);
		glTexCoord2f(1.0, 0.0);glVertex2f(1, 11);
		glTexCoord2f(1.0, 1.0);glVertex2f(1, 13);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//10 secs
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at((int)seconds/10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(1 ,13);
		glTexCoord2f(0.0, 0.0);glVertex2f(1 , 11);
		glTexCoord2f(1.0, 0.0);glVertex2f(3, 11);
		glTexCoord2f(1.0, 1.0);glVertex2f(3, 13);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//secs
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at((int)seconds%10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(3 ,13);
		glTexCoord2f(0.0, 0.0);glVertex2f(3 , 11);
		glTexCoord2f(1.0, 0.0);glVertex2f(5, 11);
		glTexCoord2f(1.0, 1.0);glVertex2f(5, 13);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//calc score digits
	if(score < 0)
		score = 0;
	int tempScore = score;
	int score10000 = tempScore/10000;
	tempScore %= 10000;
	int score1000 = tempScore/1000;
	tempScore %= 1000;
	int score100 = tempScore/100;
	tempScore %= 100;
	int score10 = tempScore/10;
	int score1 = tempScore%10;
	//draw score
	//draw 10000s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at(score10000));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(-5 ,8);
		glTexCoord2f(0.0, 0.0);glVertex2f(-5 , 6);
		glTexCoord2f(1.0, 0.0);glVertex2f(-3, 6);
		glTexCoord2f(1.0, 1.0);glVertex2f(-3, 8);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw 1000s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at(score1000));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(-3 ,8);
		glTexCoord2f(0.0, 0.0);glVertex2f(-3 , 6);
		glTexCoord2f(1.0, 0.0);glVertex2f(-1, 6);
		glTexCoord2f(1.0, 1.0);glVertex2f(-1, 8);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw 100s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at(score100));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(-1 ,8);
		glTexCoord2f(0.0, 0.0);glVertex2f(-1 , 6);
		glTexCoord2f(1.0, 0.0);glVertex2f(1, 6);
		glTexCoord2f(1.0, 1.0);glVertex2f(1, 8);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw 10s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at(score10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(1 ,8);
		glTexCoord2f(0.0, 0.0);glVertex2f(1 , 6);
		glTexCoord2f(1.0, 0.0);glVertex2f(3, 6);
		glTexCoord2f(1.0, 1.0);glVertex2f(3, 8);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw 1s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glBindTexture(GL_TEXTURE_2D, numTextures.at(score1));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(3 ,8);
		glTexCoord2f(0.0, 0.0);glVertex2f(3 , 6);
		glTexCoord2f(1.0, 0.0);glVertex2f(5, 6);
		glTexCoord2f(1.0, 1.0);glVertex2f(5, 8);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw resume button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, resumeTex);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(resumeButton.getCentre().getX() - resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() + resumeButton.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(resumeButton.getCentre().getX() - resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() - resumeButton.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(resumeButton.getCentre().getX() + resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() - resumeButton.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(resumeButton.getCentre().getX() + resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() + resumeButton.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw exit button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, exitTex);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(exitButton.getCentre().getX() - exitButton.getWidth()/2 ,exitButton.getCentre().getY() + exitButton.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(exitButton.getCentre().getX() - exitButton.getWidth()/2 ,exitButton.getCentre().getY() - exitButton.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(exitButton.getCentre().getX() + exitButton.getWidth()/2 ,exitButton.getCentre().getY() - exitButton.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(exitButton.getCentre().getX() + exitButton.getWidth()/2 ,exitButton.getCentre().getY() + exitButton.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
}

//method to calculate the level score
void EndScreen::setScore(int parTime, int deathCount, int gemCount){
	score = 5*(parTime - levelTime) + 50*gemCount - 5*deathCount;
}
//method so set the time taken for level
void EndScreen::setTime(int t){levelTime = t;}
//method to find if resume button is pressed
bool EndScreen::resumeGame(){return resume;}
//method to reset the screen
void EndScreen::reset(){
	active = false;
	exit = false;
	resume = false;
}
//method to find if buttons are being hovered over and/or pressed
void EndScreen::checkButtonPress(){
	
		//check resume button against mouse pos
		if((mousePos->x > resumeButton.getCentre().x - (resumeButton.getWidth()/2)) && (mousePos->x < resumeButton.getCentre().x + (resumeButton.getWidth()/2)) 
			&& (mousePos->y > resumeButton.getCentre().y - (resumeButton.getHeight()/2)) && (mousePos->y < resumeButton.getCentre().y + (resumeButton.getHeight()/2))){
				resumeTex = textures.at(1);//set texture to hover button
				if(*leftPressed)
					resume = true; //set resume button flag to pressed
		}
		else
			resumeTex = textures.at(0);//set normal button tex
		//check exit button against mouse pos
		if((mousePos->x > exitButton.getCentre().x - (exitButton.getWidth()/2)) && (mousePos->x < exitButton.getCentre().x + (exitButton.getWidth()/2)) 
			&& (mousePos->y > exitButton.getCentre().y - (exitButton.getHeight()/2)) && (mousePos->y < exitButton.getCentre().y + (exitButton.getHeight()/2))){
				exitTex = textures.at(3);//set hover button tex
				if(*leftPressed)
						exit = true;//set exit flag to pressed
		}
		else
			exitTex = textures.at(2);//set normal button tex

	
}
//method to load textures
void EndScreen::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	for each(char* tex in numTexNames){
		numTextures.push_back(loadPNG(tex));
	}
	exitTex = textures.at(2);//set exit texture
	resumeTex = textures.at(0);//set resume texture
}