#pragma once
#include "Platform.h"

class MovingPlatform : public Platform{
private:
	Point2d vel;
	Point2d startPos;
	Point2d endPos;
	int dir;
	bool active;
public:
	MovingPlatform();
	MovingPlatform(float w, float h, GLuint tex, Point2d v, Point2d s, Point2d e);
	void update(double deltaT);
	void setActive();
	bool isActive();
	Point2d getVel();
	int getDir();
};