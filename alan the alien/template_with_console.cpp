
//includes areas for keyboard control, mouse control, resizing the window
//and draws a spinning rectangle
#include "glew.h"
#include <windows.h>		// Header file for Windows
#include <gl\gl.h>			// Header file for the OpenGL32 Library
#include <gl\glu.h>			// Header file for the GLu32 Library
#include <vector>

#include "console.h"		// Header file for Console
#include "Point2d.h"
#include "GameObject.h"
#include "Player.h"
#include "Platform.h"
#include "Enemy.h"
#include "MovingPlatform.h"
#include "TriggerZone.h"
#include "EndDoor.h"
#include "Level.h"
#include "Menu.h"
#include "Checkpoint.h"
#include "EndScreen.h"
#include "Gem.h"

ConsoleWindow console;		//console window variable


clock_t currentTime = clock();
clock_t prevTime = clock();
double deltaT = 0;

const int VIEW_SIZE = 20;

float screenWidth=1600, screenHeight=840;
bool keys[256];
bool leftPressed = false;
bool upReleased;
Point2d mousePos;
Menu menu(&mousePos,&leftPressed);
EndScreen endScreen(100,120,&mousePos,&leftPressed);
std::vector<Level*> levels;
int currentLevel = 0;
float origAspect = screenWidth/screenHeight;
float aspect = screenWidth/screenHeight;
float widthAspect = 1;
float heightAspect = 1;
float aspectRatio = aspect/aspect;



//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void reshape(int width, int height);				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys();         //called in winmain to process keyboard input
void update(double deltaT);				//called in winmain to update variables
GLuint loadPNG(char* name);
Level* createLevel1();
Level* createLevel2();
Level* createLevel3();
void resetLevels();
void resetLevel1();
void resetLevel2();
void resetLevel3();

/*************    START OF OPENGL FUNCTIONS   ****************/
void display()									
{
	glClear(GL_COLOR_BUFFER_BIT);
	
	glLoadIdentity();
	if(menu.isActive())
		menu.draw();
	else
		if(endScreen.isActive())
			endScreen.draw();
	else
		levels.at(currentLevel)->draw();

	glFlush();
}

void update(double deltaT)
{
	if(deltaT > 0.03)//cap deltaT
		deltaT = 0.03;
	if(menu.isActive())
		menu.checkButtonPress();
	else
		if(endScreen.isActive())
			endScreen.checkButtonPress();
		else
			levels.at(currentLevel)->update(deltaT);
	
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
														// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset the current viewport

	glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
	glLoadIdentity();									// reset the top of the projection matrix to an identity matrix

	aspect = screenWidth/(screenHeight);
	widthAspect = screenWidth/1600;						//update aspect variables
	heightAspect = screenHeight/840;
	aspectRatio = origAspect/aspect;

	glOrtho(-VIEW_SIZE*aspect,VIEW_SIZE*aspect,-VIEW_SIZE,VIEW_SIZE,-(VIEW_SIZE-1),VIEW_SIZE);           // set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
	glLoadIdentity();									// Reset the top of the modelview matrix to an identity matrix
}


void init()
{
	glClearColor(0.0,0.0,0.0,0.0);
	//create levels
	levels.push_back(createLevel1());
	levels.push_back(createLevel2());
	levels.push_back(createLevel3());
	for each(Level* l in levels)//initialise levels
		l->initialise();
	//load menu textures
	menu.loadTextures();
	endScreen = EndScreen(levels.at(currentLevel)->getTime(),levels.at(currentLevel)->getParTime(),&mousePos,&leftPressed);
	endScreen.loadTextures();

}
void processKeys()
{
	if(!menu.isActive() && !endScreen.isActive() && levels.at(currentLevel)->getPlayer()->isAlive()){
		if(keys[VK_LEFT] || keys['A'])
		{
			levels.at(currentLevel)->getPlayer()->setVelX(-4.0f);//move player left
		}
		if(keys[VK_RIGHT] || keys['D'])
		{
			levels.at(currentLevel)->getPlayer()->setVelX(4.0f);//move player right
		}
		if(keys[VK_UP] || keys['W'])//make player jump
		{
			if(levels.at(currentLevel)->getPlayer()->getFalling() == false){
				levels.at(currentLevel)->getPlayer()->setVelY(10.0f);
				levels.at(currentLevel)->getPlayer()->setFalling(true);
			}
		}
	}

}

Level* createLevel1(){
	GLuint grassTop = loadPNG("Tiles/grassMid.png");
	GLuint grassCentre = loadPNG("Tiles/grassCenter.png");
	GLuint box = loadPNG("Tiles/boxAlt.png");
	Player* player = new Player(3.0,3.5,Point2d(2,2),Point2d());
	std::vector<Platform*> platforms;
	std::vector<Lava*> lava;
	std::vector<Enemy*> enemies;
	std::vector<MovingPlatform*> movingPlats;
	std::vector<TriggerZone*> triggers;
	std::vector<Checkpoint*> checkpoints;
	std::vector<Gem*> gems;
	//platforms(width,height,centre,texture)
	platforms.push_back(new Platform(4,88,Point2d(-2,40),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(4,-2),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(16,-2),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(24,2),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(32,6),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(40,2),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(50,-2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(58,2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(62,6),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(66,10),grassTop));
	platforms.push_back(new Platform(4,72,Point2d(70,32),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(32,2),grassCentre));
	platforms.push_back(new Platform(24,4,Point2d(32,-2),grassCentre));
	platforms.push_back(new Platform(12,4,Point2d(62,-2),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(64,2),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(66,6),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(12,10),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(4,14),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(4,10),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(16,22),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(22,26),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(30,26),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(26,22),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(30,18),grassCentre));
	platforms.push_back(new Platform(12,4,Point2d(38,18),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(46,18),grassCentre));
	platforms.push_back(new Platform(16,4,Point2d(52,18),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(46,22),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(58,22),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(64,22),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(66,26),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(66,30),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(62,26),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(2,26),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(24,38),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(32,42),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(54,34),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(50,34),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(38,46),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(46,50),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(46,46),grassCentre));
	platforms.push_back(new Platform(24,4,Point2d(40,38),grassCentre));
	platforms.push_back(new Platform(16,4,Point2d(44,42),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(2,54),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(6,50),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(2,50),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(16,62),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(26,66),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(26,62),grassCentre));
	platforms.push_back(new Platform(28,4,Point2d(46,62),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(66,66),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(64,62),grassCentre));
	platforms.push_back(new Platform(4,20,Point2d(82,70),grassCentre));
	platforms.push_back(new Platform(16,4,Point2d(80,46),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(74,50),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(78,62),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(88,50),grassTop));
	platforms.push_back(new Platform(4,16,Point2d(98,60),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(94,54),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(94,50),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(86,62),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(96,70),grassTop));
	platforms.push_back(new Platform(4,56,Point2d(106,64),grassCentre));
	platforms.push_back(new Platform(24,4,Point2d(92,38),grassTop));
	platforms.push_back(new Platform(4,16,Point2d(82,28),grassCentre));
	platforms.push_back(new Platform(16,4,Point2d(80,10),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(90,14),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(86,26),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(98,18),grassTop));
	platforms.push_back(new Platform(20,4,Point2d(82,-2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(90,2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(102,2),grassTop));
	platforms.push_back(new Platform(44,4,Point2d(114,-2),grassCentre));
	platforms.push_back(new Platform(28,4,Point2d(118,2),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(124,6),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(106,6),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(118,6),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(124,10),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(130,6),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(134,2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(138,-2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(146,-2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(150,-2),grassCentre));
	platforms.push_back(new Platform(4,32,Point2d(154,12),grassCentre));
	platforms.push_back(new Platform(28,4,Point2d(134,22),grassCentre));
	platforms.push_back(new Platform(4,12,Point2d(118,26),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(122,26),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(146,26),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(118,34),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(110,38),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(118,46),grassTop));
	platforms.push_back(new Platform(4,24,Point2d(122,56),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(122,70),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(110,54),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(118,62),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(130,42),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(138,50),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(142,58),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(152,66),grassTop));
	platforms.push_back(new Platform(4,32,Point2d(134,76),grassCentre));
	platforms.push_back(new Platform(4,8,Point2d(138,44),grassCentre));
	platforms.push_back(new Platform(4,16,Point2d(142,48),grassCentre));
	platforms.push_back(new Platform(4,16,Point2d(146,48),grassCentre));
	platforms.push_back(new Platform(8,24,Point2d(152,52),grassCentre));
	platforms.push_back(new Platform(4,100,Point2d(158,46),grassCentre));
	platforms.push_back(new Platform(164,80,Point2d(78,-42),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(-158,-146),grassCentre));

	//lava(width,centre)
	lava.push_back(new Lava(4,Point2d(10,-2)));
	lava.push_back(new Lava(4,Point2d(26,26)));
	lava.push_back(new Lava(8,Point2d(52,22)));
	lava.push_back(new Lava(8,Point2d(80,50)));
	lava.push_back(new Lava(8,Point2d(96,2)));
	lava.push_back(new Lava(8,Point2d(112,6)));
	lava.push_back(new Lava(4,Point2d(142,-2)));
	lava.push_back(new Lava(20,Point2d(134,26)));
	lava.push_back(new Lava(4,Point2d(146,58)));
	//enemies(width,height,centre,Xvelocity,drop)
	enemies.push_back(new Enemy(3,2,Point2d(46,1),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(40,21),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(46,65),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(82,1),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(80,13),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(128,45),2,KEY));
	//movingplats(width,height,velocity,startPos,endPos)
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(-2,0),Point2d(46,34),Point2d(10,34)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(2,0),Point2d(10,50),Point2d(30,50)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-2),Point2d(102,74),Point2d(102,42)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(150,2),Point2d(150,27)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(2,0),Point2d(130,30),Point2d(138,30)));
	//triggerzones(width,height,centre,linked platform)
	triggers.push_back(new TriggerZone(2,2,Point2d(54,37),movingPlats.at(0)));
	triggers.push_back(new TriggerZone(2,2,Point2d(42,53),movingPlats.at(1)));
	triggers.push_back(new TriggerZone(2,2,Point2d(98,73),movingPlats.at(2)));
	triggers.push_back(new TriggerZone(2,2,Point2d(146,1),movingPlats.at(3)));
	triggers.push_back(new TriggerZone(2,2,Point2d(154,29),movingPlats.at(4)));

	//checkpoints(centre)
	checkpoints.push_back(new Checkpoint(Point2d(86,30)));

	//gems(centre)
	gems.push_back(new Gem(Point2d(62,13)));
	gems.push_back(new Gem(Point2d(2,18)));
	gems.push_back(new Gem(Point2d(60,48)));
	gems.push_back(new Gem(Point2d(26,80)));
	gems.push_back(new Gem(Point2d(98,49)));
	gems.push_back(new Gem(Point2d(82,82)));
	gems.push_back(new Gem(Point2d(110,32)));
	gems.push_back(new Gem(Point2d(74,5)));
	gems.push_back(new Gem(Point2d(142,17)));
	gems.push_back(new Gem(Point2d(122,85)));
	gems.push_back(new Gem(Point2d(139,80)));
	//create level
	Level* level = new Level(Point2d(2,2),new EndDoor(Point2d(154,70)), 420, platforms, lava, enemies, triggers, movingPlats, checkpoints, gems,new Follower(Point2d(66,17)), player);
	return level;
}

Level* createLevel2(){
	GLuint grassTop = loadPNG("Tiles/grassMid.png");
	GLuint grassCentre = loadPNG("Tiles/grassCenter.png");
	GLuint box = loadPNG("Tiles/boxAlt.png");
	GLuint stoneTop = loadPNG("Tiles/stoneMid.png");
	GLuint stoneCentre = loadPNG("Tiles/stoneCenter.png");
	Player* player = new Player(3.0,3.5,Point2d(2,2),Point2d());
	std::vector<Platform*> platforms;
	std::vector<Lava*> lava;
	std::vector<Enemy*> enemies;
	std::vector<MovingPlatform*> movingPlats;
	std::vector<TriggerZone*> triggers;
	std::vector<Checkpoint*> checkpoints;
	std::vector<Gem*> gems;

	//platforms(width,height,centre,texture)
	platforms.push_back(new Platform(12,88,Point2d(-6,40),grassCentre));
	platforms.push_back(new Platform(24,4,Point2d(12,-2),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(28,2),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(28,-2),grassCentre));
	platforms.push_back(new Platform(24,4,Point2d(44,-2),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(58,6),grassTop));
	platforms.push_back(new Platform(44,8,Point2d(78,0),grassCentre));
	platforms.push_back(new Platform(12,12,Point2d(94,10),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(90,18),grassTop));
	platforms.push_back(new Platform(8,16,Point2d(96,24),grassCentre));
	platforms.push_back(new Platform(4,20,Point2d(94,42),grassCentre));
	platforms.push_back(new Platform(20,4,Point2d(66,22),grassCentre));
	platforms.push_back(new Platform(12,4,Point2d(78,26),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(56,26),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(52,34),grassTop));
	platforms.push_back(new Platform(4,8,Point2d(50,28),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(36,38),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(24,46),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(36,54),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(26,62),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(52,62),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(78,46),grassTop));
	platforms.push_back(new Platform(16,4,Point2d(84,42),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(94,54),grassTop));
	platforms.push_back(new Platform(20,4,Point2d(106,50),grassTop));
	platforms.push_back(new Platform(16,4,Point2d(124,58),grassTop));
	platforms.push_back(new Platform(4,8,Point2d(118,52),grassCentre));
	platforms.push_back(new Platform(4,44,Point2d(134,78),grassCentre));
	platforms.push_back(new Platform(12,4,Point2d(122,86),grassTop));
	platforms.push_back(new Platform(24,4,Point2d(104,78),grassTop));
	platforms.push_back(new Platform(4,8,Point2d(118,80),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(90,78),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(90,82),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(80,90),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(66,94),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(62,98),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(92,98),grassTop));
	platforms.push_back(new Platform(16,4,Point2d(100,94),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(106,98),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(106,102),grassTop));
	platforms.push_back(new Platform(20,4,Point2d(118,98),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(130,98),grassCentre));
	platforms.push_back(new Platform(20,4,Point2d(138,102),grassTop));
	platforms.push_back(new Platform(4,24,Point2d(150,112),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(152,126),grassTop));
	platforms.push_back(new Platform(4,84,Point2d(170,102),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(164,110),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(154,102),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(164,90),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(152,90),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(138,90),grassTop));
	platforms.push_back(new Platform(16,16,Point2d(144,80),grassCentre));
	platforms.push_back(new Platform(20,4,Point2d(158,62),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(146,66),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(146,62),grassCentre));
	platforms.push_back(new Platform(8,32,Point2d(152,44),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(140,46),grassTop));
	platforms.push_back(new Platform(16,4,Point2d(128,50),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(134,46),grassCentre));
	platforms.push_back(new Platform(20,4,Point2d(138,30),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(126,34),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(126,30),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(118,34),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(106,34),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(98,34),grassTop));
	platforms.push_back(new Platform(16,12,Point2d(112,26),grassCentre));
	platforms.push_back(new Platform(4,12,Point2d(106,14),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(112,10),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(122,10),grassTop));
	platforms.push_back(new Platform(32,32,Point2d(140,12),grassCentre));
	platforms.push_back(new Platform(20,12,Point2d(114,2),grassCentre));
	platforms.push_back(new Platform(8,20,Point2d(108,-14),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(98,-22),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(90,-18),stoneTop));
	platforms.push_back(new Platform(16,4,Point2d(80,-22),stoneTop));
	platforms.push_back(new Platform(12,4,Point2d(66,-18),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(90,-22),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(66,-22),stoneCentre));
	platforms.push_back(new Platform(24,16,Point2d(72,-32),stoneCentre));
	platforms.push_back(new Platform(4,36,Point2d(34,-22),stoneCentre));
	platforms.push_back(new Platform(8,4,Point2d(48,-18),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(58,-30),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(38,-30),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(50,-38),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(60,-46),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(60,-50),stoneCentre));
	platforms.push_back(new Platform(16,4,Point2d(72,-50),stoneTop));
	platforms.push_back(new Platform(4,12,Point2d(82,-46),stoneCentre));
	platforms.push_back(new Platform(24,4,Point2d(48,-54),stoneCentre));
	platforms.push_back(new Platform(8,4,Point2d(36,-50),stoneTop));
	platforms.push_back(new Platform(8,44,Point2d(36,-74),stoneCentre));
	platforms.push_back(new Platform(28,12,Point2d(18,-70),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(30,-62),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(22,-62),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(14,-62),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(6,-62),stoneTop));
	platforms.push_back(new Platform(12,4,Point2d(26,-38),stoneCentre));
	platforms.push_back(new Platform(4,12,Point2d(22,-46),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(26,-50),stoneTop));
	platforms.push_back(new Platform(32,4,Point2d(8,-50),stoneCentre));
	platforms.push_back(new Platform(20,4,Point2d(-14,-54),stoneCentre));
	platforms.push_back(new Platform(4,12,Point2d(-22,-62),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(-14,-66),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(-4,-62),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(-4,-66),stoneCentre));
	platforms.push_back(new Platform(28,8,Point2d(-14,-72),stoneCentre));
	platforms.push_back(new Platform(4,12,Point2d(-2,-82),stoneCentre));
	platforms.push_back(new Platform(16,4,Point2d(8,-86),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(18,-94),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(30,-94),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(18,-96),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(30,-96),stoneCentre));
	platforms.push_back(new Platform(16,4,Point2d(24,-100),stoneCentre));
	platforms.push_back(new Platform(32,4,Point2d(16,-104),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(14,-100),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(2,-100),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(2,-108),stoneCentre));
	platforms.push_back(new Platform(16,4,Point2d(-8,-108),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(-18,-104),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(-20,-108),stoneCentre));
	platforms.push_back(new Platform(4,36,Point2d(-26,-92),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(-10,-86),stoneTop));



	//lava(width,centre)
	lava.push_back(new Lava(28,Point2d(74,6)));
	lava.push_back(new Lava(12,Point2d(66,26)));
	lava.push_back(new Lava(12,Point2d(86,46)));
	lava.push_back(new Lava(8,Point2d(100,98)));
	lava.push_back(new Lava(8,Point2d(144,90)));
	lava.push_back(new Lava(8,Point2d(112,34)));
	lava.push_back(new Lava(4,Point2d(118,10)));
	lava.push_back(new Lava(16,Point2d(48,-50)));
	lava.push_back(new Lava(4,Point2d(26,-62)));
	lava.push_back(new Lava(4,Point2d(18,-62)));
	lava.push_back(new Lava(4,Point2d(10,-62)));
	lava.push_back(new Lava(8,Point2d(24,-96)));
	lava.push_back(new Lava(8,Point2d(8,-100)));



	//enemies(width,height,centre,Xvelocity,drop)
	enemies.push_back(new Enemy(3,2,Point2d(44,1),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(106,53),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(104,81),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(118,101),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(148,93),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(154,65),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(138,33),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(72,-47),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(-14,-63),2,KEY));
	enemies.push_back(new Enemy(3,2,Point2d(-10,-105),2,LEVER));

	//movingplats(width,height,velocity,startPos,endPos)
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(-2,0),Point2d(86,10),Point2d(66,10)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(130,62),Point2d(130,88)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(2,0),Point2d(30,98),Point2d(58,98)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(146,106),Point2d(146,126)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-1),Point2d(158,90),Point2d(158,78)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-2),Point2d(102,34),Point2d(102,-18)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-1),Point2d(122,34),Point2d(122,14)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-1),Point2d(2,-62),Point2d(2,-82)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(-22,-104),Point2d(-22,-86)));

	//triggerzones(width,height,centre,linked platform)
	triggers.push_back(new TriggerZone(2,2,Point2d(58,9),movingPlats.at(0)));
	triggers.push_back(new TriggerZone(2,2,Point2d(126,61),movingPlats.at(1)));
	triggers.push_back(new TriggerZone(2,2,Point2d(62,101),movingPlats.at(2)));
	triggers.push_back(new TriggerZone(2,2,Point2d(142,105),movingPlats.at(3)));
	triggers.push_back(new TriggerZone(2,2,Point2d(138,93),movingPlats.at(4)));
	triggers.push_back(new TriggerZone(2,2,Point2d(98,37),movingPlats.at(5)));
	triggers.push_back(new TriggerZone(2,2,Point2d(126,37),movingPlats.at(6)));
	triggers.push_back(new TriggerZone(2,2,Point2d(-2,-59),movingPlats.at(7)));
	triggers.push_back(new TriggerZone(2,2,Point2d(-18,-101),movingPlats.at(8)));

	//checkpoints(centre)
	checkpoints.push_back(new Checkpoint(Point2d(122,54)));

	//gems(centre)
	gems.push_back(new Gem(Point2d(54,30)));
	gems.push_back(new Gem(Point2d(18,70)));
	gems.push_back(new Gem(Point2d(102,66)));
	gems.push_back(new Gem(Point2d(30,106)));
	gems.push_back(new Gem(Point2d(134,126)));
	gems.push_back(new Gem(Point2d(166,106)));
	gems.push_back(new Gem(Point2d(126,54)));
	gems.push_back(new Gem(Point2d(110,14)));
	gems.push_back(new Gem(Point2d(110,18)));
	gems.push_back(new Gem(Point2d(114,14)));
	gems.push_back(new Gem(Point2d(114,18)));
	gems.push_back(new Gem(Point2d(40,-6)));
	gems.push_back(new Gem(Point2d(78,-42)));
	gems.push_back(new Gem(Point2d(-18,-58)));
	gems.push_back(new Gem(Point2d(30,-78)));
	gems.push_back(new Gem(Point2d(-6,-90)));
	gems.push_back(new Gem(Point2d(-22,-78)));
	
	//create level
	Level* level = new Level(Point2d(2,2),new EndDoor(Point2d(-6,-82)), 600, platforms, lava, enemies, triggers, movingPlats, checkpoints, gems, new Follower(Point2d(30,102)), player);
	return level;
}

Level* createLevel3(){
	GLuint grassTop = loadPNG("Tiles/grassMid.png");
	GLuint grassCentre = loadPNG("Tiles/grassCenter.png");
	GLuint box = loadPNG("Tiles/boxAlt.png");
	GLuint stoneTop = loadPNG("Tiles/stoneMid.png");
	GLuint stoneCentre = loadPNG("Tiles/stoneCenter.png");
	Player* player = new Player(3.0,3.5,Point2d(-6,-82),Point2d());
	std::vector<Platform*> platforms;
	std::vector<Lava*> lava;
	std::vector<Enemy*> enemies;
	std::vector<MovingPlatform*> movingPlats;
	std::vector<TriggerZone*> triggers;
	std::vector<Checkpoint*> checkpoints;
	std::vector<Gem*> gems;

	//platforms(width,height,centre,texture)
	platforms.push_back(new Platform(28,8,Point2d(-14,-72),stoneCentre));
	platforms.push_back(new Platform(4,12,Point2d(-2,-82),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(-10,-86),stoneTop));
	platforms.push_back(new Platform(4,36,Point2d(-26,-94),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(-6,-110),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(-14,-106),stoneTop));
	platforms.push_back(new Platform(12,4,Point2d(-18,-110),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(2,-102),stoneTop));
	platforms.push_back(new Platform(16,8,Point2d(8,-108),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(14,-102),stoneTop));
	platforms.push_back(new Platform(20,4,Point2d(42,-106),stoneTop));
	platforms.push_back(new Platform(44,4,Point2d(38,-110),stoneCentre));
	platforms.push_back(new Platform(32,20,Point2d(16,-78),stoneCentre));
	platforms.push_back(new Platform(4,36,Point2d(34,-78),stoneCentre));
	platforms.push_back(new Platform(8,4,Point2d(56,-106),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(54,-102),stoneTop));
	platforms.push_back(new Platform(12,4,Point2d(42,-94),stoneTop));
	platforms.push_back(new Platform(4,28,Point2d(58,-92),stoneCentre));
	platforms.push_back(new Platform(16,4,Point2d(52,-76),stoneTop));
	platforms.push_back(new Platform(24,20,Point2d(48,-56),stoneCentre));
	platforms.push_back(new Platform(20,4,Point2d(70,-84),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(72,-72),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(82,-84),stoneTop));
	platforms.push_back(new Platform(24,4,Point2d(72,-60),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(86,-56),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(86,-60),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(90,-60),stoneTop));
	platforms.push_back(new Platform(8,12,Point2d(88,-80),stoneCentre));
	platforms.push_back(new Platform(8,4,Point2d(88,-72),stoneTop));
	platforms.push_back(new Platform(36,4,Point2d(110,-80),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(126,-76),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(126,-72),stoneTop));
	platforms.push_back(new Platform(76,8,Point2d(98,-46),stoneCentre));
	platforms.push_back(new Platform(4,8,Point2d(130,-70),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(130,-64),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(134,-60),stoneTop));
	platforms.push_back(new Platform(20,4,Point2d(142,-64),stoneCentre));
	platforms.push_back(new Platform(28,4,Point2d(122,-52),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(150,-60),stoneTop));
	platforms.push_back(new Platform(4,28,Point2d(158,-52),stoneCentre));
	platforms.push_back(new Platform(4,28,Point2d(162,-76),stoneCentre));
	platforms.push_back(new Platform(24,4,Point2d(148,-88),stoneCentre));
	platforms.push_back(new Platform(4,24,Point2d(138,-76),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(158,-72),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(148,-76),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(140,-52),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(138,-44),stoneTop));
	platforms.push_back(new Platform(8,20,Point2d(152,-32),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(138,-28),stoneTop));
	platforms.push_back(new Platform(12,20,Point2d(130,-32),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(130,-20),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(146,-36),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(148,-20),stoneTop));
	platforms.push_back(new Platform(36,12,Point2d(122,-8),stoneCentre));
	platforms.push_back(new Platform(12,12,Point2d(110,-20),stoneCentre));
	platforms.push_back(new Platform(8,4,Point2d(112,-28),stoneCentre));
	platforms.push_back(new Platform(20,4,Point2d(94,-20),stoneCentre));
	platforms.push_back(new Platform(4,24,Point2d(82,-30),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(90,-40),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(98,-32),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(98,-36),stoneCentre));
	platforms.push_back(new Platform(8,4,Point2d(100,-40),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(102,-36),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(106,-40),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(120,-40),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(122,-32),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(118,-24),stoneTop));
	platforms.push_back(new Platform(32,4,Point2d(156,-4),stoneCentre));
	platforms.push_back(new Platform(24,4,Point2d(168,-24),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(178,-20),stoneCentre));
	platforms.push_back(new Platform(12,4,Point2d(178,-16),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(174,-12),stoneTop));
	platforms.push_back(new Platform(8,4,Point2d(180,-12),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(178,-8),stoneTop));
	platforms.push_back(new Platform(4,4,Point2d(182,-8),stoneCentre));
	platforms.push_back(new Platform(4,4,Point2d(182,-4),stoneCentre));
	platforms.push_back(new Platform(48,20,Point2d(144,8),grassCentre));
	platforms.push_back(new Platform(4,12,Point2d(182,4),grassCentre));
	platforms.push_back(new Platform(4,8,Point2d(186,14),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(174,4),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(180,12),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(186,20),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(170,16),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(166,20),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(160,24),grassTop));
	platforms.push_back(new Platform(16,4,Point2d(156,20),grassCentre));
	platforms.push_back(new Platform(8,40,Point2d(152,42),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(160,36),grassCentre));
	platforms.push_back(new Platform(20,4,Point2d(174,32),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(172,28),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(174,24),grassCentre));
	platforms.push_back(new Platform(24,4,Point2d(200,16),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(214,20),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(214,16),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(220,28),grassTop));
	platforms.push_back(new Platform(8,12,Point2d(220,20),grassCentre));
	platforms.push_back(new Platform(4,32,Point2d(226,30),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(226,48),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(216,48),grassTop));
	platforms.push_back(new Platform(4,8,Point2d(214,42),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(208,40),grassTop));
	platforms.push_back(new Platform(20,4,Point2d(198,36),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(192,40),grassTop));
	platforms.push_back(new Platform(24,4,Point2d(176,36),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(160,40),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(172,48),grassTop));
	platforms.push_back(new Platform(12,4,Point2d(182,48),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(178,52),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(186,52),grassTop));
	platforms.push_back(new Platform(16,4,Point2d(196,52),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(190,56),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(202,56),grassTop));
	platforms.push_back(new Platform(4,32,Point2d(230,62),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(230,80),grassTop));
	platforms.push_back(new Platform(4,16,Point2d(206,62),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(206,72),grassTop));
	platforms.push_back(new Platform(20,8,Point2d(218,66),grassCentre));
	platforms.push_back(new Platform(8,4,Point2d(184,64),grassTop));
	platforms.push_back(new Platform(12,12,Point2d(174,68),grassCentre));
	platforms.push_back(new Platform(16,4,Point2d(164,60),grassCentre));
	platforms.push_back(new Platform(4,36,Point2d(170,92),grassCentre));
	platforms.push_back(new Platform(16,4,Point2d(180,76),grassTop));
	platforms.push_back(new Platform(4,16,Point2d(190,82),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(190,92),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(194,84),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(174,84),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(178,92),grassTop));
	platforms.push_back(new Platform(20,4,Point2d(190,100),grassTop));
	platforms.push_back(new Platform(28,4,Point2d(186,108),grassCentre));
	platforms.push_back(new Platform(8,12,Point2d(204,112),grassCentre));
	platforms.push_back(new Platform(4,16,Point2d(206,98),grassCentre));
	platforms.push_back(new Platform(4,16,Point2d(234,74),grassCentre));
	platforms.push_back(new Platform(12,4,Point2d(238,84),grassTop));
	platforms.push_back(new Platform(8,8,Point2d(248,86),grassCentre));
	platforms.push_back(new Platform(4,24,Point2d(250,102),grassCentre));
	platforms.push_back(new Platform(4,4,Point2d(246,92),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(236,96),grassTop));
	platforms.push_back(new Platform(4,4,Point2d(218,100),grassTop));
	platforms.push_back(new Platform(8,4,Point2d(212,104),grassTop));
	platforms.push_back(new Platform(44,4,Point2d(230,116),grassCentre));

	//lava(width,centre)
	lava.push_back(new Lava(8,Point2d(-20,-106)));
	lava.push_back(new Lava(8,Point2d(8,-102)));
	lava.push_back(new Lava(16,Point2d(24,-106)));
	lava.push_back(new Lava(32,Point2d(108,-76)));
	lava.push_back(new Lava(20,Point2d(150,-84)));
	lava.push_back(new Lava(8,Point2d(112,-40)));
	lava.push_back(new Lava(20,Point2d(162,-20)));
	lava.push_back(new Lava(8,Point2d(200,40)));
	lava.push_back(new Lava(4,Point2d(182,52)));
	lava.push_back(new Lava(8,Point2d(196,56)));
	lava.push_back(new Lava(20,Point2d(218,72)));


	//enemies(width,height,centre,Xvelocity,drop)
	enemies.push_back(new Enemy(3,2,Point2d(-10,-107),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(70,-81),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(78,-57),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(144,-61),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(90,-37),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(200,19),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(164,39),2,LEVER));
	enemies.push_back(new Enemy(3,2,Point2d(188,39),2,KEY));
	enemies.push_back(new Enemy(3,2,Point2d(182,79),2,LEVER));

	//movingplats(width,height,velocity,startPos,endPos)
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(38,-90),Point2d(38,-76)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(94,-72),Point2d(94,-60)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(-2,0),Point2d(122,-72),Point2d(106,-72)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-1),Point2d(154,-60),Point2d(154,-76)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(-2,0),Point2d(170,-16),Point2d(154,-16)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,2),Point2d(222,32),Point2d(222,48)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(0,-2),Point2d(202,76),Point2d(202,60)));
	movingPlats.push_back(new MovingPlatform(4,4,box,Point2d(1,0),Point2d(210,76),Point2d(222,76)));

	//triggerzones(width,height,centre,linked platform)
	triggers.push_back(new TriggerZone(2,2,Point2d(42,-91),movingPlats.at(0)));
	triggers.push_back(new TriggerZone(2,2,Point2d(90,-69),movingPlats.at(1)));
	triggers.push_back(new TriggerZone(2,2,Point2d(90,-57),movingPlats.at(2)));
	triggers.push_back(new TriggerZone(2,2,Point2d(150,-57),movingPlats.at(3)));
	triggers.push_back(new TriggerZone(2,2,Point2d(150,-17),movingPlats.at(4)));
	triggers.push_back(new TriggerZone(2,2,Point2d(218,31),movingPlats.at(5)));
	triggers.push_back(new TriggerZone(2,2,Point2d(190,59),movingPlats.at(6)));
	triggers.push_back(new TriggerZone(2,2,Point2d(182,67),movingPlats.at(7)));

	//checkpoints(centre)
	checkpoints.push_back(new Checkpoint(Point2d(146,-72)));
	checkpoints.push_back(new Checkpoint(Point2d(158,28)));

	//gems(centre)
	gems.push_back(new Gem(Point2d(2,-90)));
	gems.push_back(new Gem(Point2d(55,-80)));
	gems.push_back(new Gem(Point2d(82,-72)));
	gems.push_back(new Gem(Point2d(106,-52)));
	gems.push_back(new Gem(Point2d(158,-68)));
	gems.push_back(new Gem(Point2d(142,-68)));
	gems.push_back(new Gem(Point2d(86,-24)));
	gems.push_back(new Gem(Point2d(142,-8)));
	gems.push_back(new Gem(Point2d(158,32)));
	gems.push_back(new Gem(Point2d(200,32)));
	gems.push_back(new Gem(Point2d(226,60)));
	gems.push_back(new Gem(Point2d(158,56)));
	gems.push_back(new Gem(Point2d(182,72)));
	gems.push_back(new Gem(Point2d(174,104)));
	gems.push_back(new Gem(Point2d(218,94)));
	gems.push_back(new Gem(Point2d(246,110)));
	//create level
	Level* level = new Level(Point2d(-6,-82),new EndDoor(Point2d(210,108)), 600, platforms, lava, enemies, triggers, movingPlats, checkpoints, gems, new Follower(Point2d(142,-68)), player);
	return level;
}

void resetLevels(){
	//if level has been completed reset it
	if(levels.at(0)->getEndDoor()->getActive())
		resetLevel1();

	if(levels.at(1)->getEndDoor()->getActive())
		resetLevel2();

	if(levels.at(2)->getEndDoor()->getActive())
		resetLevel3();
}

void resetLevel1(){
	
	Level* l = createLevel1();
	l->initialise();
	levels.insert(levels.begin(),l);
	levels.erase(levels.begin()+1);
}
void resetLevel2(){
	
	Level* l = createLevel2();
	l->initialise();
	levels.insert(levels.begin()+1,l);
	levels.erase(levels.begin()+2);
}
void resetLevel3(){
	
	Level* l = createLevel3();
	l->initialise();
	levels.insert(levels.begin()+2,l);
	levels.erase(levels.begin()+3);
}

GLuint loadPNG(char* name)
{
	// Texture loading object
	nv::Image img;

	GLuint myTextureID;

	// Return true on success
	if(img.loadImageFromFile(name))
	{
		glGenTextures(1, &myTextureID);
		glBindTexture(GL_TEXTURE_2D, myTextureID);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}

	else
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);

	return myTextureID;
}


/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop


	console.Open();

	// Create Our OpenGL Window
	if (!CreateGLWindow("Alan the alien, earthly adventures",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{

			// TIMER
			// Get the current time
			currentTime = clock();
			clock_t clockTicksTaken = currentTime - prevTime;
			deltaT =(clockTicksTaken / (double) CLOCKS_PER_SEC);


			if(keys[VK_ESCAPE] && !endScreen.isActive())
				menu.setActive();

			if(levels.at(currentLevel)->getEndDoor()->getActive() && levels.at(currentLevel)->getFollowerStatus() && !endScreen.isActive()){
				endScreen.setActive();
				endScreen.setTime(levels.at(currentLevel)->getTime());
				endScreen.setScore(levels.at(currentLevel)->getParTime(),levels.at(currentLevel)->getPlayer()->getDeathCount(),levels.at(currentLevel)->getPlayer()->getGemCount());

			}
			if(endScreen.isActive()){
				if(endScreen.resumeGame()){
					endScreen.reset();
					if(currentLevel >= levels.size()-1){
						menu.setActive();
						resetLevels();
						currentLevel = 0;
					}
					else
						currentLevel++;
				}
					
				if(endScreen.exitGame())
					done = true;
			}
			if(menu.isActive()){
				if(menu.exitGame())
					done = true;
			}

			if(menu.levelSelected()){
					menu.setLevelSelectedFalse();
					int level = menu.getLevel();
					if(level == 0){
						currentLevel = 0;
						if(levels.at(currentLevel)->getEndDoor()->getActive())
							resetLevel1();
					}
					if(level == 1){
						currentLevel = 1;
						if(levels.at(currentLevel)->getEndDoor()->getActive())
							resetLevel2();
					}
					if(level == 2){
						currentLevel = 2;
						if(levels.at(currentLevel)->getEndDoor()->getActive())
							resetLevel3();
					}
				}
				
			processKeys();			//process keyboard

			update(deltaT);					// update variables

						// Advance timer
			prevTime = currentTime;					
			

			display();					// Draw The Scene
			
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			prevTime = clock();
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
				mousePos.x = (((LOWORD(lParam) - screenWidth/2))/(VIEW_SIZE*widthAspect*aspectRatio));          
				mousePos.y = (((screenHeight/2 - HIWORD(lParam)))/(VIEW_SIZE*heightAspect));
	            leftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            leftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
				mousePos.x = (((LOWORD(lParam) - screenWidth/2))/(VIEW_SIZE*widthAspect*aspectRatio));          
				mousePos.y = (((screenHeight/2 - HIWORD(lParam)))/(VIEW_SIZE*heightAspect));

			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(hRC=wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!wglMakeCurrent(hDC,hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();
	
	return true;									// Success
}



