#include "EndDoor.h"
//default constuctor
EndDoor::EndDoor(){
	width = 4;
	height = 4;
	centre = Point2d();
	rad = sqrt(20.0f);
	texName = "Tiles/door_closedMid.png";
	active = false;
}
//constuctor
EndDoor::EndDoor(Point2d c){
	width = 4;
	height = 4;
	centre = c;
	rad = sqrt(20.0f);
	texName = "Tiles/door_closedMid.png";
	active = false;
}
//draw method
void EndDoor::draw(){
	//set door texture and draw the door
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	//texture set to clamp to edge
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(1, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(1, 1);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
}
//load door teture
void EndDoor::loadTexture(){
	this->texture = loadPNG(texName);
}
//make door active
void EndDoor::setActive(){active = true;}
//get door status
bool EndDoor::getActive(){return active;}