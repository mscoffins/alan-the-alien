#pragma once
#include "GameObject.h"

class EndDoor : public GameObject{
private:
	char* texName;
	GLuint texture;
	bool active;

public:
	EndDoor();
	EndDoor(Point2d c);
	void draw();
	void loadTexture();
	void setActive();
	bool getActive();
};