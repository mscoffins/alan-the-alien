#pragma once
#include "GameObject.h"

class Checkpoint : public GameObject{
private:
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	GLuint currentTex;
	bool active;
	float count;

public:
	Checkpoint(Point2d c);
	void update(double deltaT);
	void draw();
	bool isActive();
	void setActive();
	void loadTextures();

};