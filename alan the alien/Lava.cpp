#include "Lava.h"
//constructor
Lava::Lava(float w, Point2d centre){
	width = w;
	height = 4;
	this->centre = centre;
	texName = "Tiles/liquidLava.png";
	topTexName = "Tiles/liquidLavaTop_mid.png";
	xMove = 0;
}
//draw method
void Lava::draw(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, topTex);//set top texture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//repeat in x
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);//dont repeat in y
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw lava top, setting texture to move by movement amount
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0 + xMove, 0.75);glVertex2f(centre.getX() - width/2 ,centre.getY() + 2);
		glTexCoord2f(0.0 + xMove, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY());
		glTexCoord2f(width/4 + xMove, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY());
		glTexCoord2f(width/4 + xMove, 0.75);glVertex2f(centre.getX() + width/2 ,centre.getY() + 2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);//set bottom texture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//repeat in x
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);//dont repeat in y
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw lava bottom, setting texture to move by movement amount
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0 + xMove, 1);glVertex2f(centre.getX() - width/2 ,centre.getY());
		glTexCoord2f(0.0 + xMove, 0.25);glVertex2f(centre.getX() - width/2 ,centre.getY()-2);
		glTexCoord2f(width/4 + xMove, 0.25);glVertex2f(centre.getX() + width/2 ,centre.getY()-2);
		glTexCoord2f(width/4 + xMove, 1);glVertex2f(centre.getX() + width/2 ,centre.getY());
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();
}
//update method
void Lava::update(double deltaT){
	//increase move amount
	xMove += deltaT;
	if(xMove > width)
		xMove = 0;
}
//method to load textures
void Lava::loadTextures(){
	texture = loadPNG(texName);
	topTex = loadPNG(topTexName);
}