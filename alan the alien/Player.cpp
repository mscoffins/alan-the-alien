#include "Player.h"
#include <math.h>
#include <iostream>
//default constructor
Player::Player(){
	this->width = 1;
	this->height = 1;
	this->centre = Point2d();
	this->rad = sqrt(2.0f);
	this->falling = true;
	texNames.push_back("Player/p1_walk/PNG/p1_walk01.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk02.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk03.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk04.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk05.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk06.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk07.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk08.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk09.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk10.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk11.png");
	texNames.push_back("Player/p1_hurt.png");
	texNames.push_back("Player/p1_jump.png");
	texNames.push_back("Player/p1_stand.png");
	count = 1;
	dir = 1;
	leverCount = 0;
	keyCount = 0;
	gemCount = 0;
	deathCount = 0;
	alive = true;
	follower = false;
}
//constructor
Player::Player(float w, float h, Point2d c, Point2d v){
	width = w;
	height = h;
	centre = c;
	vel = v;
	rad = sqrt((w/2)*(w/2) + (h/2)*(h/2));
	falling = true;
	texNames.push_back("Player/p1_walk/PNG/p1_walk01.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk02.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk03.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk04.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk05.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk06.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk07.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk08.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk09.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk10.png");
	texNames.push_back("Player/p1_walk/PNG/p1_walk11.png");
	texNames.push_back("Player/p1_hurt.png");
	texNames.push_back("Player/p1_jump.png");
	texNames.push_back("Player/p1_stand.png");
	hudTexNames.push_back("HUD/hud_0.png");
	hudTexNames.push_back("HUD/hud_1.png");
	hudTexNames.push_back("HUD/hud_2.png");
	hudTexNames.push_back("HUD/hud_3.png");
	hudTexNames.push_back("HUD/hud_4.png");
	hudTexNames.push_back("HUD/hud_5.png");
	hudTexNames.push_back("HUD/hud_6.png");
	hudTexNames.push_back("HUD/hud_7.png");
	hudTexNames.push_back("HUD/hud_8.png");
	hudTexNames.push_back("HUD/hud_9.png");
	hudTexNames.push_back("HUD/hud_x.png");
	hudTexNames.push_back("HUD/hud_keyYellow.png");
	hudTexNames.push_back("Items/switchMid.png");
	hudTexNames.push_back("Items/gemBlue.png");
	count = 1;
	dir = 1;
	leverCount = 0;
	keyCount = 0;
	gemCount = 0;
	deathCount = 0;
	alive = true;
	follower = false;
}
//method to draw player object in level, called in level draw function
void Player::draw(){

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	//set texture properties
	glBindTexture(GL_TEXTURE_2D, currentTex);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw player
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(dir, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(dir, 1.0);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();

}

//method to draw the players heads up display on the screen, called in levels draw function
void Player::drawHud(Point2d cameraPos){
	//draw key in top left corner
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(11));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 26 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 26 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 24 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 24 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw key multiple sign
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 24 ,cameraPos.getY() + 17.5);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 24 ,cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 23 ,cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 23 ,cameraPos.getY() + 17.5);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw key count
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	

	glBindTexture(GL_TEXTURE_2D, hudTextures.at(keyCount));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 23 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 23 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 21 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 21 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw lever
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(12));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 17 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 17 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 15 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 15 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw lever multiple sign
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 15, cameraPos.getY() + 17.5);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 15, cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 14, cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 14, cameraPos.getY() + 17.5);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw number of levers
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	

	glBindTexture(GL_TEXTURE_2D, hudTextures.at(leverCount));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 14 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 14 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 12 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 12 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw gem symbol
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(13));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.25, 0.75);glVertex2f(cameraPos.getX() - 8 ,cameraPos.getY() + 18);
		glTexCoord2f(0.25, 0.25);glVertex2f(cameraPos.getX() - 8 ,cameraPos.getY() + 16);
		glTexCoord2f(0.75, 0.25);glVertex2f(cameraPos.getX() - 6 ,cameraPos.getY() + 16);
		glTexCoord2f(0.75, 0.75);glVertex2f(cameraPos.getX() - 6 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw gem multiple sign
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 6, cameraPos.getY() + 17.5);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 6, cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 5, cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 5, cameraPos.getY() + 17.5);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw gem count 10s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	

	glBindTexture(GL_TEXTURE_2D, hudTextures.at(gemCount/10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 5 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 5 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 3 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 3 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw gem count 1s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	

	glBindTexture(GL_TEXTURE_2D, hudTextures.at(gemCount%10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 3 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 3 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() - 1 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() - 1 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw death symbol
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, textures.at(11));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 3 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 3 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 5 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 5 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw death multiple sign
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, hudTextures.at(10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 5, cameraPos.getY() + 17.5);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 5, cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 6, cameraPos.getY() + 16.5);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 6, cameraPos.getY() + 17.5);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw death count 10s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	

	glBindTexture(GL_TEXTURE_2D, hudTextures.at(deathCount/10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 6 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 6 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 8 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 8 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw death count 1s
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	

	glBindTexture(GL_TEXTURE_2D, hudTextures.at(deathCount%10));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 8 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 8 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 10 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 10 ,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
}
//method to update the players position and texture 
void Player::update(double deltaT){
	if(centre.y < -300)
		alive = false;
	//move player based on velocity and deltaT
	centre.setX(centre.getX() + (vel.getX())*(deltaT*5));
	centre.setY(centre.getY() + (vel.getY())*(deltaT*5));
	//set players direction
	if(vel.getX() < 0)
		dir = -1;
	if(vel.getX() > 0)
		dir = 1;	
	//determine if player if falling
	if(vel.getY() < -1)
		falling = true;
	//update players vertical velocity with gravity
	vel.setY(vel.getY()-(deltaT*25));
	//cap players vertical velocity
	if(vel.y < - 30)
		vel.setY(-30);
	//set count to 1 if it is less than 1
	if(count < 1)
		count = 1;
	//if player is moving, update its moving texture based on the count var
	if(vel.getX() != 0){
		if(count <= 4)
			currentTex = textures.at(0);
		if(count > 4 && count <= 8)
			currentTex = textures.at(1);
		if(count > 8 && count <= 12)
			currentTex = textures.at(2);
		if(count > 12 && count <= 16)
			currentTex = textures.at(3);
		if(count > 16 && count <= 20)
			currentTex = textures.at(4);
		if(count > 20 && count <= 24)
			currentTex = textures.at(5);
		if(count > 24 && count <= 28)
			currentTex = textures.at(6);
		if(count > 28 && count <= 32)
			currentTex = textures.at(7);
		if(count > 32 && count <= 36)
			currentTex = textures.at(8);
		if(count > 36 && count <= 40)
			currentTex = textures.at(9);
		if(count > 40 && count <= 44)
			currentTex = textures.at(10);
	}
	//if player is stationary set its texture to the idle texture
	if(vel.getX() == 0){
		currentTex = textures.at(13);
	}
	//if player if falling set its texture to the falling texture
	if(falling == true && vel.y != -5){
		currentTex = textures.at(12);
	}
	//update count variable
	if(count < 44)
		count += (deltaT*100);
	else
		count = 1;
	//set x velocity to 0
	vel.setX(0);
	//if player is dead drop follower and set texture to dead texture
	if(!alive){
		currentTex = textures.at(11);
		follower = false;
	}
	//cap death count to 99
	if(deathCount >= 99)
		deathCount = 99;
	
}
//getters and setters
void Player::setVel(Point2d v){this->vel = v;}
Point2d Player::getVel(){return vel;}
bool Player::getFalling(){return falling;}
int Player::getDir(){return dir;}
bool Player::isAlive(){return alive;}
void Player::setFalling(bool f){falling = f;}
void Player::setVelX(float x){vel.setX(x);}
void Player::setVelY(float y){vel.setY(y);}
void Player::setTexture(GLuint tex){textures.push_back(tex);}
int Player::getDeathCount(){return deathCount;}
int Player::getGemCount(){return deathCount;}
void Player::pickupFollower(){follower = true;}
void Player::dropFollower(){follower = false;}
double Player::getCount(){return count;}

//method to respawn player
void Player::respawn(Point2d spawnPoint){
	centre = spawnPoint;
	alive = true;
}
//method to load player textures
void Player::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	this->currentTex = textures.back();
	for each(char* tex in hudTexNames){
		hudTextures.push_back(loadPNG(tex));
	}
}
//collision for player against a platform
void Player::platformCollision(Platform* o){
	//check circle collision
	bool cCol = false;
	float o1x = this->centre.getX();
	float o2x = o->getCentre().getX();
	float o1y = this->centre.getY();
	float o2y = o->getCentre().getY();
	if(((o1x-o2x)*(o1x-o2x)+(o1y-o2y)*(o1y-o2y))< (this->rad+o->getRad())*(this->rad+o->getRad()))
		cCol = true;	
	//if circle collision is true do box collision
	if(cCol == true){
		//calculate the min and max values for both objects bounding boxes
		float xMinA = this->centre.getX()-this->width/2;
		float xMaxB = o->getCentre().getX()+o->getWidth()/2;
		float xMaxA = this->centre.getX()+this->width/2;
		float xMinB = o->getCentre().getX()-o->getWidth()/2;
		float yMinA = this->centre.getY()-this->height/2;
		float yMaxB = o->getCentre().getY()+o->getHeight()/2;
		float yMaxA = this->centre.getY()+this->height/2;
		float yMinB = o->getCentre().getY()-o->getHeight()/2;
		//create boolean variables for collisions on each side of player
		bool checkLeft = false;
		bool checkRight = false;
		bool checkTop = false;
		bool checkBottom = false;
		//calculate the distabce overlap for each side
		float reactLeft = xMaxB-xMinA;
		float reactRight = xMaxA - xMinB;
		float reactTop = yMaxA - yMinB;
		float reactBottom = yMaxB - yMinA;
		//work out which sides of player have overlaps
		if(xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		if(xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		if(yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		if(yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		//if any instance resulting in the left side of the player colliding with the object are true push the player back out to the right
		if(checkLeft&&checkTop&&checkBottom || checkLeft&&checkTop&&(reactTop>reactLeft) || checkLeft&&checkBottom&&(reactBottom>reactLeft)){

			centre.setX(this->getCentre().getX()+(reactLeft+0.01));
			this->setVelX(0);
		}
		//if any instance resulting in the right side of the player colliding with the object are true push the player back out to the left
		if(checkRight&&checkTop&&checkBottom || checkRight&&checkTop&&(reactTop>reactRight) ||checkRight&&checkBottom&&(reactBottom>reactRight)){

			centre.setX(this->getCentre().getX()-(reactRight+0.01));
			this->setVelX(0);
		}
		//if any instance resulting in the bottom side of the player colliding with the object are true push the player back out up and set the payer to not be falling
		if(checkRight&&checkLeft&&checkBottom||checkBottom&&checkRight&&(reactBottom<reactRight) || checkLeft&&checkBottom&&(reactBottom<reactLeft)){
			centre.setY(this->getCentre().getY()+reactBottom+0.01);
			if(vel.getY()<-1)
				this->setVelY(-vel.getY()*0.2);
			else
				this->setVelY(0);
			this->setFalling(false);
		}
		//if any instance resulting in the top side of the player colliding with the object are true push the player back out down and set the player to falling
		if(checkRight&&checkLeft&&checkTop || checkRight&&checkTop&&(reactTop<reactRight) || checkTop&&checkLeft&&(reactTop<reactLeft)){

			centre.setY(centre.getY()-(reactTop+0.01));
			this->setVelY(0);
			this->setFalling(true);
		}
		
		
		
	}

}
//enemy collision method
void Player::enemyCollision(Enemy* e){
	//check circle collision
	bool cCol = false;
	float o1x = this->centre.getX();
	float o2x = e->getCentre().getX();
	float o1y = this->centre.getY();
	float o2y = e->getCentre().getY();
	if(((o1x-o2x)*(o1x-o2x)+(o1y-o2y)*(o1y-o2y))< (this->rad+e->getRad())*(this->rad+e->getRad()))
		cCol = true;	
	//if circle collision is true do box collision
	if(cCol == true){
		//calculate the min and max values for both objects bounding boxes
		float xMinA = this->centre.getX()-this->width/2;
		float xMaxB = e->getCentre().getX()+e->getWidth()/2;
		float xMaxA = this->centre.getX()+this->width/2;
		float xMinB = e->getCentre().getX()-e->getWidth()/2;
		float yMinA = this->centre.getY()-this->height/2;
		float yMaxB = e->getCentre().getY()+e->getHeight()/2;
		float yMaxA = this->centre.getY()+this->height/2;
		float yMinB = e->getCentre().getY()-e->getHeight()/2;
		//create boolean variables for collisions on each side of player
		bool checkLeft = false;
		bool checkRight = false;
		bool checkTop = false;
		bool checkBottom = false;
		//calculate the distabce overlap for each side
		float reactLeft = xMaxB-xMinA;
		float reactRight = xMaxA - xMinB;
		float reactTop = yMaxA - yMinB;
		float reactBottom = yMaxB - yMinA;
		//work out which sides of player have overlaps
		if(xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		if(xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		if(yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		if(yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		//left
		if(checkLeft&&checkTop&&checkBottom || checkLeft&&checkTop&&(reactTop>reactLeft) || checkLeft&&checkBottom&&(reactBottom>reactLeft)){
			currentTex = textures.at(11);
			alive = false;//kill player
			deathCount++;
		}
		//right
		if(checkRight&&checkTop&&checkBottom || checkRight&&checkTop&&(reactTop>reactRight) ||checkRight&&checkBottom&&(reactBottom>reactRight)){
			currentTex = textures.at(11);
			alive = false;//kill player
			deathCount++;
		}
		//bottom
		if(checkRight&&checkLeft&&checkBottom||checkBottom&&checkRight&&(reactBottom<reactRight) || checkLeft&&checkBottom&&(reactBottom<reactLeft)){

			centre.setY(this->getCentre().getY()+reactBottom+0.01);
			e->kill();//kill enemy
			Item drop = e->getDrop();//pick up item drop
			if(drop == Item::LEVER)
				leverCount++;
			if(drop == Item::KEY)
				keyCount++;
			//bounce mechanic
			if(vel.getY()<-1)
				this->setVelY(-vel.getY()*0.2);
			else
				this->setVelY(0);
			this->setFalling(false);//stop player falling
		}
		//top
		if(checkRight&&checkLeft&&checkTop || checkRight&&checkTop&&(reactTop<reactRight) || checkTop&&checkLeft&&(reactTop<reactLeft)){
			currentTex = textures.at(11);
			alive = false;//kill player
			deathCount++;
		}
		
		
		
	}

}
//moving playform collision
void Player::movingPlatformCollision(MovingPlatform* mp, double deltaT){

		//calculate the min and max values for both objects bounding boxes
		float xMinA = this->centre.getX()-this->width/2;
		float xMaxB = mp->getCentre().getX()+mp->getWidth()/2;
		float xMaxA = this->centre.getX()+this->width/2;
		float xMinB = mp->getCentre().getX()-mp->getWidth()/2;
		float yMinA = this->centre.getY()-this->height/2;
		float yMaxB = mp->getCentre().getY()+mp->getHeight()/2;
		float yMaxA = this->centre.getY()+this->height/2;
		float yMinB = mp->getCentre().getY()-mp->getHeight()/2;

		float yMaxBOverlap = yMaxB + mp->getHeight() / 2;

		//create boolean variables for collisions on each side of player
		bool checkLeft = false;
		bool checkRight = false;
		bool checkTop = false;
		bool checkBottom = false;
		bool checkOverlap = false;
		//calculate the distabce overlap for each side
		float reactLeft = xMaxB-xMinA;
		float reactRight = xMaxA - xMinB;
		float reactTop = yMaxA - yMinB;
		float reactBottom = yMaxB - yMinA;
		//work out which sides of player have overlaps
		if(xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		if(xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		if(yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		if(yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		if (xMinA <= xMaxB && xMaxA >= xMinB && yMinA >= yMinB && yMinA <= yMaxBOverlap)
			checkOverlap = true;
		//check left
		if(checkLeft&&checkTop&&checkBottom || checkLeft&&checkTop&&(reactTop>reactLeft) || checkLeft&&checkBottom&&(reactBottom>reactLeft)){

			centre.setX(this->getCentre().getX()+(reactLeft+0.01));
			this->setVelX(0);
		}
		//check right
		if(checkRight&&checkTop&&checkBottom || checkRight&&checkTop&&(reactTop>reactRight) ||checkRight&&checkBottom&&(reactBottom>reactRight)){

			centre.setX(this->getCentre().getX()-(reactRight+0.01));
			this->setVelX(0);
		}
		//check bottom
		if(checkRight&&checkLeft&&checkBottom||checkBottom&&checkRight&&(reactBottom<reactRight) || checkLeft&&checkBottom&&(reactBottom<reactLeft)){
			this->setFalling(false);
			centre.setY(centre.getY()+reactBottom + 0.01);
			this->setVelY(0);
			
		}
		//check top
		if(checkRight&&checkLeft&&checkTop || checkRight&&checkTop&&(reactTop<reactRight) || checkTop&&checkLeft&&(reactTop<reactLeft)){
			if(mp->getVel().getY() != 0 && !this->falling){//if player isnt falling kill player
				this->alive = false;
				currentTex = textures.at(11);
				deathCount++;
			}
			else{
				centre.setY(centre.getY()-(reactTop+0.01));
				this->setVelY(0);
				this->setFalling(true);
			}
		}

		if (mp->isActive() && checkOverlap) {
			if (mp->getVel().x != 0) {
				centre.setX(centre.x + ((mp->getVel().x*mp->getDir()) * (deltaT * 5)));
				//vel.setY(-5);//set vel so player will collide again next frame to stick to platform
			}
			if (mp->getVel().y != 0) {
				centre.setY(centre.y + ((mp->getVel().y*mp->getDir()) * (deltaT * 5)));
				//vel.setY(-5);
			}
			currentTex = textures.at(13);//set idle texture
		}
		
		
		
	
}
//trigger zone collision
void Player::triggerCollision(TriggerZone* t){
	//AABB collision used
	float xMinA = this->centre.getX()-this->width/2;
	float xMaxB = t->getCentre().getX()+t->getWidth()/2;
	float xMaxA = this->centre.getX()+this->width/2;
	float xMinB = t->getCentre().getX()-t->getWidth()/2;
	float yMinA = this->centre.getY()-this->height/2;
	float yMaxB = t->getCentre().getY()+t->getHeight()/2;
	float yMaxA = this->centre.getY()+this->height/2;
	float yMinB = t->getCentre().getY()-t->getHeight()/2;
	if((xMinA < xMaxB && xMaxA > xMinB && yMinA < yMaxB && yMaxA > yMinB) && leverCount > 0){
		//if colliding and player has levers activate trigger zone and decrement lever count
		t->setActive();
		leverCount--;
	}
}
//end door collision
void Player::endCollision(EndDoor* e){
	//AABB collision used
	float xMinA = this->centre.getX()-this->width/2;
	float xMaxB = e->getCentre().getX()+e->getWidth()/2;
	float xMaxA = this->centre.getX()+this->width/2;
	float xMinB = e->getCentre().getX()-e->getWidth()/2;
	float yMinA = this->centre.getY()-this->height/2;
	float yMaxB = e->getCentre().getY()+e->getHeight()/2;
	float yMaxA = this->centre.getY()+this->height/2;
	float yMinB = e->getCentre().getY()-e->getHeight()/2;
	if((xMinA < xMaxB && xMaxA > xMinB && yMinA < yMaxB && yMaxA > yMinB) && keyCount > 0 && follower){
		//if colliding and player has follower and key set to active
		e->setActive();
		keyCount--;
	}

}
//lava collision
void Player::lavaCollision(Lava* l){
	//AABB collision used
	float xMinA = this->centre.getX()-this->width/2;
	float xMaxB = l->getCentre().getX()+l->getWidth()/2;
	float xMaxA = this->centre.getX()+this->width/2;
	float xMinB = l->getCentre().getX()-l->getWidth()/2;
	float yMinA = this->centre.getY()-this->height/2;
	float yMaxB = l->getCentre().getY()+l->getHeight()/2;
	float yMaxA = this->centre.getY()+this->height/2;
	float yMinB = l->getCentre().getY()-l->getHeight()/2;
	if((xMinA < xMaxB && xMaxA > xMinB && yMinA < yMaxB && yMaxA > yMinB)){
		//if colliding kill player
		alive = false;
		currentTex = textures.at(11);
		deathCount++;
	}
}
//checkpoint collision
void Player::checkpointCollision(Checkpoint* c){
	//AABB collison used
	float xMinA = this->centre.getX()-this->width/2;
	float xMaxB = c->getCentre().getX()+c->getWidth()/2;
	float xMaxA = this->centre.getX()+this->width/2;
	float xMinB = c->getCentre().getX()-c->getWidth()/2;
	float yMinA = this->centre.getY()-this->height/2;
	float yMaxB = c->getCentre().getY()+c->getHeight()/2;
	float yMaxA = this->centre.getY()+this->height/2;
	float yMinB = c->getCentre().getY()-c->getHeight()/2;
	if((xMinA < xMaxB && xMaxA > xMinB && yMinA < yMaxB && yMaxA > yMinB) && follower){
		//if colliding and player has follower activate checkpoint
		c->setActive();
	}
}
//gem collision
void Player::gemCollision(Gem* g){
	//AABB collision used
	float xMinA = this->centre.getX()-this->width/2;
	float xMaxB = g->getCentre().getX()+g->getWidth()/2;
	float xMaxA = this->centre.getX()+this->width/2;
	float xMinB = g->getCentre().getX()-g->getWidth()/2;
	float yMinA = this->centre.getY()-this->height/2;
	float yMaxB = g->getCentre().getY()+g->getHeight()/2;
	float yMaxA = this->centre.getY()+this->height/2;
	float yMinB = g->getCentre().getY()-g->getHeight()/2;
	if((xMinA < xMaxB && xMaxA > xMinB && yMinA < yMaxB && yMaxA > yMinB)){
		//if colliding pick up the gem
		g->pickUp();
		gemCount++;
	}
}
