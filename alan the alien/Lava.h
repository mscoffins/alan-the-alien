#pragma once
#include "Platform.h"

class Lava : public Platform{
private:
	char* texName;
	char* topTexName;
	GLuint topTex;
	float xMove;

public:
	Lava(float w, Point2d centre);
	void draw();
	void update(double deltaT);
	void loadTextures();
};