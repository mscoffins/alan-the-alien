#pragma once
#include <vector>
#include <ctime>
#include "Point2d.h"
#include "GameObject.h"
#include "Player.h"
#include "Platform.h"
#include "Enemy.h"
#include "MovingPlatform.h"
#include "TriggerZone.h"
#include "EndDoor.h"
#include "Checkpoint.h"
#include "Gem.h"
#include "Follower.h"
using namespace std;

class Level{
private:
	Point2d startpos;
	Point2d respawnPos;
	Point2d cameraPos;
	Point2d cameraVel;
	EndDoor* end;
	vector<Platform*> platforms;
	vector<Lava*> lava;
	vector<Enemy*> enemies;
	vector<TriggerZone*> triggerZones;
	vector<MovingPlatform*> movingPlatforms;
	vector<Checkpoint*> checkpoints;
	vector<Gem*> gems;
	Player* player;
	Follower* follower;
	GLuint bgTop;
	GLuint bgBottom;
	float respawnCount;
	clock_t startTime;
	clock_t prevTime;
	int timeInact;
	int time;
	int parTime;
	vector<char*> respawnTexNames;
	vector<GLuint> respawnTextures;
	vector<char*> timeTexNames;
	vector<GLuint> timeTextures;


public:
	Level(Point2d start, EndDoor* end, int pt, vector<Platform*> platforms, vector<Lava*> lava, vector<Enemy*> enemies,vector<TriggerZone*> triggerZones,vector<MovingPlatform*> movingPlatforms, vector<Checkpoint*> checkpoints, vector<Gem*> gems,Follower* follower, Player* player);
	void update(double deltaT);
	void draw();
	void drawRespawn();
	void drawTime();
	void initialise();
	void drawBackground();
	void setRespawnPos();
	int getTime();
	int getParTime();
	bool getFollowerStatus();
	Player* getPlayer();
	EndDoor* getEndDoor();
	Point2d getCameraPos();
	Point2d getCameraVel();
	GLuint loadPNG(char* name);
};