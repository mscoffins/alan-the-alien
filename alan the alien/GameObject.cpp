#include "GameObject.h"
#include <math.h>
//default constructor
GameObject::GameObject(){
	this->width = 10;
	this->height = 10;
	this->centre = Point2d();
	this->rad = sqrt(200.0f);

}
//constructor
GameObject::GameObject(float w, float h, Point2d c){
	width = w;
	height = h;
	centre = c;
	rad = sqrt((w/2)*(w/2) + (h/2)*(h/2));
}
//draw method
void GameObject::draw(){
	glPushMatrix();
	glBegin(GL_POLYGON);
		glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();
	glPopMatrix();
}
//getters and setters
float GameObject::getWidth(){return width;}

float GameObject::getHeight(){return height;}

float GameObject::getRad(){return rad;}

Point2d GameObject::getCentre(){return centre;}

void GameObject::setWidth(float w){this->width = w;}

void GameObject::setHeight(float h){this->height = h;}

void GameObject::setRad(float r){this->rad = r;}

void GameObject::setCentre(Point2d c){this->centre = c;}
//texture loading method
GLuint GameObject::loadPNG(char* name){
	// Texture loading object
	nv::Image img;

	GLuint myTextureID;

	// Return true on success
	if(img.loadImageFromFile(name))
	{
		glGenTextures(1, &myTextureID);
		glBindTexture(GL_TEXTURE_2D, myTextureID);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}

	else
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);

	return myTextureID;
}