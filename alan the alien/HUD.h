#include <vector>
#include "Player.h"

class HUD{
private:
	int leverCount;
	int keyCount;
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	Player* player;

public:
	HUD();
	void draw();
	void update();
	void loadTextures();
	void addLever();
	void addKey();
};