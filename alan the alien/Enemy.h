#pragma once
#include "GameObject.h"
#include <vector>
#include "glew.h"
#include "Platform.h"

enum Item {LEVER,KEY};


class Enemy : public GameObject{
	
private:
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	GLuint currentTex;
	float vel;
	double count;
	Item drop;
	bool alive;

public:
	
	Enemy();
	Enemy(float w, float h, Point2d c, float v, Item drop);
	void draw();
	void update(double deltaT);
	float getVel();
	int getCount();
	Item getDrop();
	void setVel(float v);
	void checkCollision(GameObject* o);
	void loadTextures();
	void kill();
	bool isDead();
};