#include "Follower.h"
//constructor
Follower::Follower(Point2d c){
	centre = c;
	width = 2;
	height = 2;
	rad = sqrt(8.0f);
	texNames.push_back("flying/frame-1.png");
	texNames.push_back("flying/frame-2.png");
	texNames.push_back("flying/frame-3.png");
	texNames.push_back("flying/frame-4.png");
	active = false;
	dir = 1;
}
//draw method
void Follower::draw(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D, currentTex);//set texture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//repeat in x
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);//dont repeat in y
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw follower
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(dir, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(dir, 1.0);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
}
//update follower
void Follower::update(double deltaT, Player* p){
	if(active){//if following player
		if(!p->isAlive())//if player is dead stop following
			active = false;
		dir = p->getDir();//set direction of travel to players movement direction
		if(centre.x != (p->getCentre().x -(p->getWidth()*p->getDir()))|| centre.y != p->getCentre().y ){//if not in target position(just behind player) move fractionally closer
			centre.setX(centre.getX() + ((deltaT*5)*(p->getCentre().x -(p->getWidth()*p->getDir()) - centre.getX())));//set x to current x plus a fraction of x distance from player
			centre.setY(centre.getY() + ((deltaT*5)*(p->getCentre().y - centre.getY())));//set y yo current y plus fraction of y distance from player centre
		}
		
		
	}
	//update texture based off players count var
	if(p->getCount() < 11)
		this->currentTex = textures.at(0);
	if(p->getCount() >= 11 && p->getCount() < 22)
		this->currentTex = textures.at(1);
	if(p->getCount() >= 22 && p->getCount() < 33)
		this->currentTex = textures.at(2);
	if(p->getCount() >= 33)
		this->currentTex = textures.at(3);
	
}
//getters and setters
bool Follower::getActive(){return active;}
void Follower::setActive(){ active = true;}
//method to load textures
void Follower::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	currentTex = textures.at(0);
}
//method to find if follower his hitting the player, if so player picks up the follower
void Follower::playerCollision(Player* p){
	float xMinA = this->centre.getX()-this->width/2;
	float xMaxB = p->getCentre().getX()+p->getWidth()/2;
	float xMaxA = this->centre.getX()+this->width/2;
	float xMinB = p->getCentre().getX()-p->getWidth()/2;
	float yMinA = this->centre.getY()-this->height/2;
	float yMaxB = p->getCentre().getY()+p->getHeight()/2;
	float yMaxA = this->centre.getY()+this->height/2;
	float yMinB = p->getCentre().getY()-p->getHeight()/2;
	if((xMinA < xMaxB && xMaxA > xMinB && yMinA < yMaxB && yMaxA > yMinB)){
		active = true;
		p->pickupFollower();
	}
}
//collision detection with platforms
void Follower::PlatformCollision(Platform* p){
	//initial circle collision check
	bool cCol = false;
	float o1x = this->centre.getX();
	float o2x = p->getCentre().getX();
	float o1y = this->centre.getY();
	float o2y = p->getCentre().getY();
	if(((o1x-o2x)*(o1x-o2x)+(o1y-o2y)*(o1y-o2y))< (this->rad+p->getRad())*(this->rad+p->getRad()))
		cCol = true;	

	if(cCol == true){//if circl collision is true do box collision
		//calc min and max values
		float xMinA = this->centre.getX()-this->width/2;
		float xMaxB = p->getCentre().getX()+p->getWidth()/2;
		float xMaxA = this->centre.getX()+this->width/2;
		float xMinB = p->getCentre().getX()-p->getWidth()/2;
		float yMinA = this->centre.getY()-this->height/2;
		float yMaxB = p->getCentre().getY()+p->getHeight()/2;
		float yMaxA = this->centre.getY()+this->height/2;
		float yMinB = p->getCentre().getY()-p->getHeight()/2;

		bool checkLeft = false;
		bool checkRight = false;
		bool checkTop = false;
		bool checkBottom = false;
		//calculate reaction values
		float reactLeft = xMaxB-xMinA;
		float reactRight = xMaxA - xMinB;
		float reactTop = yMaxA - yMinB;
		float reactBottom = yMaxB - yMinA;
		//left overlap
		if(xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		//right overlap
		if(xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		//bottom overlap
		if(yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		//top overlap
		if(yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		//left collision
		if(checkLeft&&checkTop&&checkBottom || checkLeft&&checkTop&&(reactTop>reactLeft) || checkLeft&&checkBottom&&(reactBottom>reactLeft)){

			centre.setX(this->getCentre().getX()+(reactLeft+0.01));
		}
		//right collision
		if(checkRight&&checkTop&&checkBottom || checkRight&&checkTop&&(reactTop>reactRight) ||checkRight&&checkBottom&&(reactBottom>reactRight)){

			centre.setX(this->getCentre().getX()-(reactRight+0.01));
		}
		//bottom collision
		if(checkRight&&checkLeft&&checkBottom||checkBottom&&checkRight&&(reactBottom<reactRight) || checkLeft&&checkBottom&&(reactBottom<reactLeft)){
			centre.setY(this->getCentre().getY()+reactBottom+0.01);
		}
		//top collision
		if(checkRight&&checkLeft&&checkTop || checkRight&&checkTop&&(reactTop<reactRight) || checkTop&&checkLeft&&(reactTop<reactLeft)){

			centre.setY(centre.getY()-(reactTop+0.01));
		}
		
		
		
	}
}