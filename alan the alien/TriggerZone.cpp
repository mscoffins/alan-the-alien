#include "TriggerZone.h"
//constructor
TriggerZone::TriggerZone(float w, float h, Point2d c, MovingPlatform* p){
	width = w;
	height = h;
	centre = c;
	rad = sqrt((w*w)+(h*h));
	linkedPlat = p;
	active = false;
	texNames.push_back("Items/switchLeft.png");
	texNames.push_back("Items/rock.png");
	texNames.push_back("Items/switchRight.png");
}
//draw method
void TriggerZone::draw(){
	if(active)//if active set texture based on platforms direction
		currentTex = textures.at(1+linkedPlat->getDir());
	
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, currentTex);//set texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);//dont repeat texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw trigger zone
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
}
//set active
void TriggerZone::setActive(){
	active = true;
	linkedPlat->setActive();//activate linked platform
}
//load textures method
void TriggerZone::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	this->currentTex = textures.at(1);
}
//get status
bool TriggerZone::isActive(){return active;}