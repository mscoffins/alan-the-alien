#include "Level.h"
#include<iostream>
//constructor
Level::Level(Point2d start, EndDoor* end, int pt, vector<Platform*> platforms, vector<Lava*> lava, vector<Enemy*> enemies,vector<TriggerZone*> triggerZones,vector<MovingPlatform*> movingPlatforms, vector<Checkpoint*> checkpoints, vector<Gem*> gems, Follower* follower, Player* player){
	this->startpos = start;
	this->respawnPos = start;
	this->cameraPos = start;
	this->cameraVel = Point2d();
	this->end = end;
	this->platforms = platforms;
	this->lava = lava;
	this->enemies = enemies;
	this->triggerZones = triggerZones;
	this->movingPlatforms =  movingPlatforms;
	this->checkpoints = checkpoints;
	this->gems = gems;
	this->player = player;
	this->follower = follower;
	startTime = clock();
	prevTime = clock();
	timeInact = 0;
	time = 0;
	parTime = pt;
	respawnCount = 2.99;
	respawnTexNames.push_back("HUD/hud_0.png");
	respawnTexNames.push_back("HUD/hud_1.png");
	respawnTexNames.push_back("HUD/hud_2.png");
	respawnTexNames.push_back("HUD/hud_3.png");
	respawnTexNames.push_back("HUD/respawn.png");
	timeTexNames.push_back("HUD/hud_0.png");
	timeTexNames.push_back("HUD/hud_1.png");
	timeTexNames.push_back("HUD/hud_2.png");
	timeTexNames.push_back("HUD/hud_3.png");
	timeTexNames.push_back("HUD/hud_4.png");
	timeTexNames.push_back("HUD/hud_5.png");
	timeTexNames.push_back("HUD/hud_6.png");
	timeTexNames.push_back("HUD/hud_7.png");
	timeTexNames.push_back("HUD/hud_8.png");
	timeTexNames.push_back("HUD/hud_9.png");
	timeTexNames.push_back("HUD/colon.png");
}
//update method
void Level::update(double deltaT){
	//update level time
	clock_t currentTime = clock();
	clock_t clockTicksTakenInact = currentTime - prevTime;
	clock_t clockTicksTaken = currentTime - startTime;
	prevTime = clock();
	timeInact +=(int) (clockTicksTakenInact / (double) CLOCKS_PER_SEC);
	time =(int) (clockTicksTaken / (double) CLOCKS_PER_SEC) - timeInact;

	

	if(player->isAlive()){
		player->update(deltaT);//update player
		for each(Enemy* e in enemies){//check players collion against all alive enemies
			if(!e->isDead())
				player->enemyCollision(e);
		}

		for each(Platform* p in platforms){
			for each(Enemy* e in enemies){//check each alive enemies collision with platforms
				if(!e->isDead()){
					e->checkCollision(p);
					e->checkCollision(player);		
				}
			}
		
			player->platformCollision(p);//check players collison with platforms

		}

		for each(Enemy* e in enemies){//update enemies
			e->update(deltaT);
		}
		for each(TriggerZone* t in triggerZones){
			if(!t->isActive())
				player->triggerCollision(t);//check players collision with inactive trigger zones
		}
		
		for each(MovingPlatform* mp in movingPlatforms){//check players collsion with moving playforms and update them
			player->movingPlatformCollision(mp, deltaT);
			mp->update(deltaT);
		}

		for each(Lava* l in lava){//update lava and check players collision with it
			l->update(deltaT);
			player->lavaCollision(l);
		}

		for each(Checkpoint* c in checkpoints){
			c->update(deltaT);//update checkpoint and check players collsion with it
			player->checkpointCollision(c);
			if(c->isActive())
				respawnPos = c->getCentre();//set players respawn point
		}

		for each(Gem* g in gems){//check players collision with gems not picked up
			if(!g->isPickedUp())
				player->gemCollision(g);
			
		}
		
		player->endCollision(end);//check end door colision
		if(!follower->getActive())//check player collision with follower and follower collision with platforms
			follower->playerCollision(player);
			for each(Platform* p in platforms){
				if(follower->getActive())
					follower->PlatformCollision(p);
		}
		
		

		follower->update(deltaT,player);//updae follower
		if(cameraPos.x != player->getCentre().x || cameraPos.y != player->getCentre().y ){//update camera position if not centred on player
			cameraPos.setX(cameraPos.getX() + ((deltaT*2.5)*(player->getCentre().x - cameraPos.getX())));//move camera closer to player in x
			cameraPos.setY(cameraPos.getY() + ((deltaT*2.5)*(player->getCentre().y - cameraPos.getY())));//move camrea closer to player in y
		}
	}
	else{//if player is dead update respawn values
		respawnCount -= deltaT;
		if(respawnCount <= 0){//respawn player
			player->dropFollower();
			player->respawn(respawnPos);
			respawnCount = 2.99;
		}
	}

}
//draw method
void Level::draw(){
	//set window to centre on camera position
	gluLookAt(cameraPos.getX(),cameraPos.getY(),20,cameraPos.getX(),cameraPos.getY(),0,0,1,0);
	//draw level background
	drawBackground();
	//draw enemies
	for each(Enemy* e in enemies){
			e->draw();
	}
	//draw platforms
	for each(Platform* p in platforms)
		p->draw();
	//draw trigger zones
	for each(TriggerZone* t in triggerZones)
		t->draw();
	//draw moving platforms
	for each(MovingPlatform* mp in movingPlatforms)
		mp->draw();
	//draw checkpoints
	for each(Checkpoint* c in checkpoints){
		c->draw();
	}
	
	//draw gems
	for each(Gem* g in gems){
		if(!g->isPickedUp())
			g->draw();
	}
	//draw follower
	follower->draw();
	//draw player
	player->draw();
	//draw end door
	end->draw();
	//draw lava
	for each(Lava* l in lava){
		l->draw();
	}
	//draw hud
	player->drawHud(cameraPos);
	drawTime();
	
	
	
	if(!player->isAlive())//if player is dead draw respawn countdown
		drawRespawn();
	
	
}
//method to draw respawn
void Level::drawRespawn(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	//draw "respawn in"
	glBindTexture(GL_TEXTURE_2D, respawnTextures.at(4));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() - 12 ,cameraPos.getY() + 2);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() - 12 ,cameraPos.getY() - 3);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() ,cameraPos.getY() - 3);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() ,cameraPos.getY() + 2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw count
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, respawnTextures.at((int)respawnCount+1));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 1 ,cameraPos.getY() + 2);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 1 ,cameraPos.getY() - 2);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 5,cameraPos.getY() - 2);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 5,cameraPos.getY() + 2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
}
//draw level time
void Level::drawTime(){
	//calc mins and seconds
	int tempTime = 0;
	int mins = time/60;
	int seconds = time%60;
	//draw 10 mins
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, timeTextures.at((int)mins/10));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 18 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 18 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 20,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 20,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw mins
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, timeTextures.at((int)mins%10));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 20 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 20 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 22,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 22,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw colon
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, timeTextures.at(10));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 22 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 22 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 23,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 23,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw 10s seconds
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, timeTextures.at((int)seconds/10));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 23 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 23 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 25,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 25,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw seconds
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, timeTextures.at((int)seconds%10));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(cameraPos.getX() + 25 ,cameraPos.getY() + 18);
		glTexCoord2f(0.0, 0.0);glVertex2f(cameraPos.getX() + 25 ,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 0.0);glVertex2f(cameraPos.getX() + 27,cameraPos.getY() + 16);
		glTexCoord2f(1.0, 1.0);glVertex2f(cameraPos.getX() + 27,cameraPos.getY() + 18);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
};

//initialise lvel method
void Level::initialise(){
	glClearColor(0.0,0.0,0.0,0.0);
	//load background textures
	bgTop = loadPNG("bg.png");
	bgBottom = loadPNG("bg_castle.png");
	//load all level textures
	for each(Enemy* enemy in enemies){
		enemy->loadTextures();
	}
	for each(TriggerZone* t in triggerZones){
		t->loadTextures();
	}
	
	for each(Lava* l in lava){
		l->loadTextures();
	}
	for each(Checkpoint* c in checkpoints){
		c->loadTextures();
	}
	for each(Gem* g in gems){
		g->loadTexture();
	}
	end->loadTexture();
	player->loadTextures();
	follower->loadTextures();
	for each(char* tex in respawnTexNames)
		respawnTextures.push_back(loadPNG(tex));
	for each(char* tex in timeTexNames)
		timeTextures.push_back(loadPNG(tex));
}
//draw background method
void Level::drawBackground(){
	//draw above ground background
	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D, bgTop);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 720);glVertex2f(- 1080/2 , 720/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(- 1080/2 ,0);
		glTexCoord2f(1080, 0.0);glVertex2f(1080/2 ,0);
		glTexCoord2f(1080, 720);glVertex2f(1080/2 ,720/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);



	//draw bellow ground background 
	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D, bgBottom);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 720/8);glVertex2f(- 1080/2 , 0);
		glTexCoord2f(0.0, 0.0);glVertex2f(- 1080/2 ,-720/2);
		glTexCoord2f(1080/8, 0.0);glVertex2f(1080/2 ,-720/2);
		glTexCoord2f(1080/8, 720/8);glVertex2f(1080/2 ,0);
	glEnd();

	glDisable(GL_TEXTURE_2D);


}
//get player
Player* Level::getPlayer(){return player;}
//get end door
EndDoor* Level::getEndDoor(){return end;}
//get camera pos and vel
Point2d Level::getCameraPos(){return cameraPos;}
Point2d Level::getCameraVel(){return cameraVel;}
//get time and par time
int Level::getTime(){return time;}
int Level::getParTime(){return parTime;}
//get follower status
bool Level::getFollowerStatus(){return follower->getActive();}
//load texture method
GLuint Level::loadPNG(char* name)
{
	// Texture loading object
	nv::Image img;

	GLuint myTextureID;

	// Return true on success
	if(img.loadImageFromFile(name))
	{
		glGenTextures(1, &myTextureID);
		glBindTexture(GL_TEXTURE_2D, myTextureID);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}

	else
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);

	return myTextureID;
}