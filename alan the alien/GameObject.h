#pragma once
#include <windows.h>	
#include "glew.h"
#include <gl\gl.h>			
#include <gl\glu.h>
#include "Point2d.h"
#include "nvImage.h"

class GameObject
{
protected:
	float width;
	float height;
	Point2d centre;
	float rad;
	
public:
	GameObject();
	GameObject(float w, float h, Point2d c);
	void draw();
	float getWidth();
	float getHeight();
	float getRad();
	Point2d getCentre();
	void setWidth(float w);
	void setHeight(float h);
	void setRad(float r);
	void setCentre(Point2d c);
	GLuint loadPNG(char* name);
};
