#pragma once
#include <vector>
#include "Point2d.h"
#include "MovingPlatform.h"
#include "GameObject.h"

class TriggerZone : public GameObject{
private:
	MovingPlatform* linkedPlat;
	bool active;
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	GLuint currentTex;
public:
	TriggerZone(float w, float h, Point2d c, MovingPlatform* p);
	void draw();
	void setActive();
	void loadTextures();
	bool isActive();
};