#include "MovingPlatform.h"
//default constructor
MovingPlatform::MovingPlatform(){
	width = 1;
	height = 1;
	centre = Point2d();
	texture = loadPNG("Tiles/boxAlt.png");
	vel = Point2d();
	startPos = Point2d();
	endPos = Point2d();
	dir = 1;
	active = false;
}
//constructor
MovingPlatform::MovingPlatform(float w, float h, GLuint tex, Point2d v, Point2d s, Point2d e){
	width = w;
	height = h;
	centre = s;
	texture = tex;
	vel = v;
	startPos = s;
	endPos = e;
	active = false;
}
//update method
void MovingPlatform::update(double deltaT){
	if(active){//if platform is active move it
		//work out which direction the platform should be moving
		if(startPos.x > endPos.x || startPos.y > endPos.y){
			if(centre.x >= startPos.x && centre.y >= startPos.y)
				dir = 1;
			if(centre.x <= endPos.x && centre.y <= endPos.y)
				dir = -1;
		}
		else{
			if(centre.x <= startPos.x && centre.y <= startPos.y)
				dir = 1;
			if(centre.x >= endPos.x && centre.y >= endPos.y)
				dir = -1;
		}

		centre.x = centre.x + ((vel.x*dir) * (deltaT*5));//update x position
		centre.y = centre.y + ((vel.y*dir) * (deltaT*5));//update y position



	}


}
//make platform active
void MovingPlatform::setActive(){
	active = true;
}
//getters
Point2d MovingPlatform::getVel(){return vel;}

bool MovingPlatform::isActive(){return active;}

int MovingPlatform::getDir(){return dir;}