#pragma once
#include "GameObject.h"
#include "Player.h"

class Follower : public GameObject{
private:
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	GLuint currentTex;
	bool active;
	int dir;

public:
	Follower(Point2d c);
	void draw();
	void update(double deltaT, Player* p);
	bool getActive();
	void setActive();
	void loadTextures();
	void playerCollision(Player* p);
	void PlatformCollision(Platform* p);
};