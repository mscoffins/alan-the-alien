#pragma once

class Point2d{
	public:
		float x;
		float y;
		Point2d();
		Point2d(float x, float y);
		float getX();
		float getY();
		void setX(float x);
		void setY(float y);
		Point2d operator+(Point2d v);
		Point2d operator-(Point2d v);
		Point2d operator*(Point2d v);
		Point2d operator/(Point2d v);
		Point2d operator*(float v);
		Point2d operator/(float v);
};

inline bool operator==(Point2d a, Point2d b);