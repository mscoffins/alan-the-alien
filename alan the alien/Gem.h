#pragma once
#include "GameObject.h"

class Gem : public GameObject{
private:
	char* texName;
	GLuint texture;
	bool pickedUp;
public:
	Gem(Point2d c);
	void draw();
	void loadTexture();
	void pickUp();
	bool isPickedUp();
};