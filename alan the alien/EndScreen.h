#pragma once
#include "Menu.h"

class EndScreen : public Menu{
private:
	int levelTime;
	int score;
	bool resume;
	std::vector<char*> numTexNames;
	std::vector<GLuint> numTextures;
	GLuint resumeTex;
	GLuint exitTex;

public:
	EndScreen();
	EndScreen(int levelTime, int parTime, Point2d* mousePos, bool* leftPressed);
	void draw();
	void setScore(int parTime, int deathCount, int gemCount);
	void setTime(int t);
	void checkButtonPress();
	void reset();
	bool resumeGame();
	void loadTextures();
};