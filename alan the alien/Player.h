#pragma once
#include "glew.h"
#include "GameObject.h"
#include <vector>
#include "Enemy.h"
#include "Platform.h"
#include "MovingPlatform.h"
#include "TriggerZone.h"
#include "EndDoor.h"
#include "Lava.h"
#include "Checkpoint.h"
#include "Gem.h"

class Player : public GameObject{
private:
	Point2d vel;
	bool falling;
	GLuint currentTex;
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	std::vector<char*> hudTexNames;
	std::vector<GLuint> hudTextures;
	double count;
	int dir;
	int leverCount;
	int keyCount;
	int gemCount;
	int deathCount;
	bool alive;
	bool follower;

public:
	Player();
	Player(float w, float h, Point2d c, Point2d v);
	void draw();
	void drawHud(Point2d cameraPos);
	Point2d getVel();
	bool getFalling();
	int getDir();
	int getDeathCount();
	int getGemCount();
	double getCount();
	void setFalling(bool f);
	void setVel(Point2d v);
	void platformCollision(Platform* p);
	void lavaCollision(Lava* l);
	void movingPlatformCollision(MovingPlatform* mp, double deltaT);
	void enemyCollision(Enemy* e);
	void triggerCollision(TriggerZone* t);
	void checkpointCollision(Checkpoint* c);
	void gemCollision(Gem* g);
	void endCollision(EndDoor* e);
	void update(double deltaT);
	void setVelX(float x);
	void setVelY(float y);
	void setTexture(GLuint tex);
	char* getTexName(int i);
	void loadTextures();
	bool isAlive();
	void pickupFollower();
	void respawn(Point2d spawnPoint);
	void dropFollower();
};