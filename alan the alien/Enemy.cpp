#include <math.h>
#include "Enemy.h"
#include <iostream>
//default constructor
Enemy::Enemy(){
	width = 4;
	height = 2;
	centre = Point2d();
	rad = sqrt(20.0f);
	drop = Item::LEVER;
	alive = true;
	count = 0;
	vel = 5;
	texNames.push_back("Enemies/slimeWalk1.png");
	texNames.push_back("Enemies/slimeWalk2.png");
	texNames.push_back("Enemies/slimeDead.png");
}
//constructor
Enemy::Enemy(float w, float h, Point2d c, float v, Item drop){
	width = w;
	height = h;
	centre = c;
	rad = sqrt((w/2)*(w/2) + (h/2)*(h/2));//calc radius
	vel = v;
	this->drop = drop;
	count = 0;
	alive = true;
	texNames.push_back("Enemies/slimeWalk1.png");
	texNames.push_back("Enemies/slimeWalk2.png");
	texNames.push_back("Enemies/slimeDead.png");
}
//draw method
void Enemy::draw(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, currentTex);//assign texture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//repeat in x
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);//dont repeat in y
	

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//enalble transparency
	//draw enemy
	glBegin(GL_POLYGON);
	if(vel <= 0){//facing left
		glTexCoord2f(0.0, 1.0);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	}
	else{//facing right
		glTexCoord2f(0.0, 1.0);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(-1.0, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(-1.0, 1.0);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	}
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
}
//method to update enemys position
void Enemy::update(double deltaT){
	if(alive){//only update position if enemy is alive
		centre.setX(centre.getX() + (vel)*(deltaT*5));//move enemy

		//cycle texture based on count variable
		if(count < 0)
			count = 0;

		if(count <= 1)
			currentTex = textures.at(0);
		if(count > 1 && count <= 2)
			currentTex = textures.at(1);
		//update count
		if(count < 2)
			count+=(deltaT*5);
		else
			count = 0;
	}
	
}
//getters and setters
float Enemy::getVel(){return vel;}
int Enemy::getCount(){return count;}
Item Enemy::getDrop(){return drop;}
void Enemy::setVel(float v){vel = v;}
//check collision against an object
void Enemy::checkCollision(GameObject* o){
	//circle collision
	bool cCol = false;
	float o1x = this->centre.getX();
	float o2x = o->getCentre().getX();
	float o1y = this->centre.getY();
	float o2y = o->getCentre().getY();
	if(((o1x-o2x)*(o1x-o2x)+(o1y-o2y)*(o1y-o2y))< (this->rad+o->getRad())*(this->rad+o->getRad()))
		cCol = true;	

	if(cCol == true){//if circle collision is true do AABB collision
		//calc min and max values for both objects
		float xMinA = this->centre.getX()-this->width/2;
		float xMaxB = o->getCentre().getX()+o->getWidth()/2;
		float xMaxA = this->centre.getX()+this->width/2;
		float xMinB = o->getCentre().getX()-o->getWidth()/2;
		float yMinA = this->centre.getY()-this->height/2;
		float yMaxB = o->getCentre().getY()+o->getHeight()/2;
		float yMaxA = this->centre.getY()+this->height/2;
		float yMinB = o->getCentre().getY()-o->getHeight()/2;

		bool checkLeft = false;
		bool checkRight = false;
		bool checkTop = false;
		bool checkBottom = false;
		//calck reaction values
		float reactLeft = xMaxB-xMinA;
		float reactRight = xMaxA - xMinB;
		float reactTop = yMaxA - yMinB;
		float reactBottom = yMaxB - yMinA;
		//left side overlap
		if(xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		//right side overlap
		if(xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		//bottom overlap
		if(yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		//top overlap
		if(yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		//left collision
		if(checkLeft&&checkTop&&checkBottom || checkLeft&&checkTop&&(reactTop>reactLeft) || checkLeft&&checkBottom&&(reactBottom>reactLeft)){
			centre.setX(this->getCentre().getX()+(reactLeft+0.01));
			this->setVel(-vel);
		}
		//right collision
		if(checkRight&&checkTop&&checkBottom || checkRight&&checkTop&&(reactTop>reactRight) ||checkRight&&checkBottom&&(reactBottom>reactRight)){
			centre.setX(this->getCentre().getX()-(reactRight+0.01));
			this->setVel(-vel);
		}

	}
}
//method to load enemy textures
void Enemy::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	this->currentTex = textures.back();
}
//method to kill enemy
void Enemy::kill(){
	height /= 2;
	centre.y -= height/2;
	alive = false;
	currentTex = textures.at(2);
}
//method to find if enemy is dead
bool Enemy::isDead(){return !alive;}