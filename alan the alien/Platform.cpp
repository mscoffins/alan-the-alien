#include "Platform.h"
//default constrcutor
Platform::Platform(){
	this->width = 1;
	this->height = 1;
	this->centre = Point2d(0,0);
	this->rad = sqrt(2.0f);
	this->texture = loadPNG("Tiles/grassMid.png");
}
//constructor
Platform::Platform(float w, float h, Point2d c, GLuint tex){
	width = w;
	height = h;
	centre = c;
	rad = sqrt((w/2)*(w/2) + (h/2)*(h/2));
	this->texture = tex;
}
//draw method
void Platform::draw(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, texture);//set texture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//repeat in x
	if(height > 4)//repeat in y only if height is greater than 4
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	else
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw platform
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, height/4);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(width/4, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(width/4, height/4);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();
}
