#include "Menu.h"
#include <iostream>
//default constructor
Menu::Menu(){
	texNames.push_back("Menu/title.png");
	texNames.push_back("Menu/resumeButton.png");
	texNames.push_back("Menu/resumeButton2.png");
	texNames.push_back("Menu/exitButton.png");
	texNames.push_back("Menu/exitButton2.png");
	texNames.push_back("HUD/hud_1.png");
	texNames.push_back("HUD/hud_1v2.png");
	texNames.push_back("HUD/hud_2.png");
	texNames.push_back("HUD/hud_2v2.png");
	texNames.push_back("HUD/hud_3.png");
	texNames.push_back("HUD/hud_3v2.png");
	texNames.push_back("bg_castle.png");
	levelSelect = false;
	exit = false;
	active = true;
	resumeButton = GameObject(10,2.5,Point2d(0,-5));
	exitButton = GameObject(10,2.5,Point2d(0,-10));
}
//constructor
Menu::Menu(Point2d* mousePos, bool* leftPressed){
	this->mousePos = mousePos;
	this->leftPressed = leftPressed;
	texNames.push_back("Menu/title.png");
	texNames.push_back("Menu/resumeButton.png");
	texNames.push_back("Menu/resumeButton2.png");
	texNames.push_back("Menu/exitButton.png");
	texNames.push_back("Menu/exitButton2.png");
	texNames.push_back("HUD/hud_1.png");
	texNames.push_back("HUD/hud_1v2.png");
	texNames.push_back("HUD/hud_2.png");
	texNames.push_back("HUD/hud_2v2.png");
	texNames.push_back("HUD/hud_3.png");
	texNames.push_back("HUD/hud_3v2.png");
	texNames.push_back("bg_castle.png");
	levelSelect = false;
	exit = false;
	active = true;
	resumeButton = GameObject(10,2.5,Point2d(0,-10));
	exitButton = GameObject(10,2.5,Point2d(0,-15));
	l1Button = GameObject(4,4,Point2d(-8,-4));
	l2Button = GameObject(4,4,Point2d(0,-4));
	l3Button = GameObject(4,4,Point2d(8,-4));


}
//draw menu
void Menu::draw(){
	//draw background
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	
	glBindTexture(GL_TEXTURE_2D, textures.at(11));
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 4.0);glVertex2f(-40,40);
		glTexCoord2f(0.0, 0.0);glVertex2f(-40,-40);
		glTexCoord2f(4.0, 0.0);glVertex2f(40,-40);
		glTexCoord2f(4.0, 4.0);glVertex2f(40,40);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw title
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, textures.at(0));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(-8,16);
		glTexCoord2f(0.0, 0.0);glVertex2f(-8,4);
		glTexCoord2f(1.0, 0.0);glVertex2f(8,4);
		glTexCoord2f(1.0, 1.0);glVertex2f(8,16);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw level 1 button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, l1Texture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(l1Button.getCentre().getX() - l1Button.getWidth()/2 ,l1Button.getCentre().getY() + l1Button.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(l1Button.getCentre().getX() - l1Button.getWidth()/2 ,l1Button.getCentre().getY() - l1Button.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(l1Button.getCentre().getX() + l1Button.getWidth()/2 ,l1Button.getCentre().getY() - l1Button.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(l1Button.getCentre().getX() + l1Button.getWidth()/2 ,l1Button.getCentre().getY() + l1Button.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw level 2 button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, l2Texture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(l2Button.getCentre().getX() - l2Button.getWidth()/2 ,l2Button.getCentre().getY() + l2Button.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(l2Button.getCentre().getX() - l2Button.getWidth()/2 ,l2Button.getCentre().getY() - l2Button.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(l2Button.getCentre().getX() + l2Button.getWidth()/2 ,l2Button.getCentre().getY() - l2Button.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(l2Button.getCentre().getX() + l2Button.getWidth()/2 ,l2Button.getCentre().getY() + l2Button.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw level 3 button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, l3Texture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(l3Button.getCentre().getX() - l3Button.getWidth()/2 ,l3Button.getCentre().getY() + l3Button.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(l3Button.getCentre().getX() - l3Button.getWidth()/2 ,l3Button.getCentre().getY() - l3Button.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(l3Button.getCentre().getX() + l3Button.getWidth()/2 ,l3Button.getCentre().getY() - l3Button.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(l3Button.getCentre().getX() + l3Button.getWidth()/2 ,l3Button.getCentre().getY() + l3Button.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw resume button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, resumeTexture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(resumeButton.getCentre().getX() - resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() + resumeButton.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(resumeButton.getCentre().getX() - resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() - resumeButton.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(resumeButton.getCentre().getX() + resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() - resumeButton.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(resumeButton.getCentre().getX() + resumeButton.getWidth()/2 ,resumeButton.getCentre().getY() + resumeButton.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();
	//draw exit button
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, exitTexture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1.0);glVertex2f(exitButton.getCentre().getX() - exitButton.getWidth()/2 ,exitButton.getCentre().getY() + exitButton.getHeight()/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(exitButton.getCentre().getX() - exitButton.getWidth()/2 ,exitButton.getCentre().getY() - exitButton.getHeight()/2);
		glTexCoord2f(1.0, 0.0);glVertex2f(exitButton.getCentre().getX() + exitButton.getWidth()/2 ,exitButton.getCentre().getY() - exitButton.getHeight()/2);
		glTexCoord2f(1.0, 1.0);glVertex2f(exitButton.getCentre().getX() + exitButton.getWidth()/2 ,exitButton.getCentre().getY() + exitButton.getHeight()/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
	glPopMatrix();



}
//get active status
bool Menu::isActive(){
	return active;
}
//make active
void Menu::setActive(){
	active = true;
}
//get exit game flag
bool Menu::exitGame(){
	return exit;
}
//get level selected
bool Menu::levelSelected(){
	return levelSelect;
}
//get level select flag
void Menu::setLevelSelectedFalse(){
	levelSelect = false;
}
//get level num
int Menu::getLevel(){
	return level;
}
//check button collison with mouse
void Menu::checkButtonPress(){
	//check mouse col on level 1 button
	if((mousePos->x > l1Button.getCentre().x - (l1Button.getWidth()/2)) && (mousePos->x < l1Button.getCentre().x + (l1Button.getWidth()/2)) 
		&& (mousePos->y > l1Button.getCentre().y - (l1Button.getHeight()/2)) && (mousePos->y < l1Button.getCentre().y + (l1Button.getHeight()/2))){
			l1Texture = textures.at(6);
		if(*leftPressed){
			active = false;
			levelSelect = true;
			level = 0;
		}
	}
	else{
		l1Texture = textures.at(5);
	}
	//check mouse col on level 2 button
	if((mousePos->x > l2Button.getCentre().x - (l2Button.getWidth()/2)) && (mousePos->x < l2Button.getCentre().x + (l2Button.getWidth()/2)) 
		&& (mousePos->y > l2Button.getCentre().y - (l2Button.getHeight()/2)) && (mousePos->y < l2Button.getCentre().y + (l2Button.getHeight()/2))){
			l2Texture = textures.at(8);
		if(*leftPressed){
			active = false;
			levelSelect = true;
			level = 1;
		}
	}
	else{
		l2Texture = textures.at(7);
	}
	//check mouse col on level 3 button
	if((mousePos->x > l3Button.getCentre().x - (l3Button.getWidth()/2)) && (mousePos->x < l3Button.getCentre().x + (l3Button.getWidth()/2)) 
		&& (mousePos->y > l3Button.getCentre().y - (l3Button.getHeight()/2)) && (mousePos->y < l3Button.getCentre().y + (l3Button.getHeight()/2))){
			l3Texture = textures.at(10);
		if(*leftPressed){
			active = false;
			levelSelect = true;
			level = 2;
		}
	}
	else{
		l3Texture = textures.at(9);
	}
	//check mouse col on resume button
	if((mousePos->x > resumeButton.getCentre().x - (resumeButton.getWidth()/2)) && (mousePos->x < resumeButton.getCentre().x + (resumeButton.getWidth()/2)) 
		&& (mousePos->y > resumeButton.getCentre().y - (resumeButton.getHeight()/2)) && (mousePos->y < resumeButton.getCentre().y + (resumeButton.getHeight()/2))){
			resumeTexture = textures.at(2);
		if(*leftPressed)
			active = false;
	}
	else{
		resumeTexture = textures.at(1);
	}
	//check mouse col on exit button
	if((mousePos->x > exitButton.getCentre().x - (exitButton.getWidth()/2)) && (mousePos->x < exitButton.getCentre().x + (exitButton.getWidth()/2)) 
		&& (mousePos->y > exitButton.getCentre().y - (exitButton.getHeight()/2)) && (mousePos->y < exitButton.getCentre().y + (exitButton.getHeight()/2))){
			exitTexture = textures.at(4);
		if(*leftPressed)
			exit = true;
	}
	else{
		exitTexture = textures.at(3);
	}

	
}
//load textures method
void Menu::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	resumeTexture = textures.at(1);
	exitTexture = textures.at(3);
	l1Texture = textures.at(5);
	l2Texture = textures.at(7);
	l3Texture = textures.at(9);
}

GLuint Menu::loadPNG(char* name){
	// Texture loading object
	nv::Image img;

	GLuint myTextureID;

	// Return true on success
	if(img.loadImageFromFile(name))
	{
		glGenTextures(1, &myTextureID);
		glBindTexture(GL_TEXTURE_2D, myTextureID);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}

	else
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);

	return myTextureID;
}