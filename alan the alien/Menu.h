#pragma once
#include <windows.h>	
#include "glew.h"
#include <gl\gl.h>			
#include <gl\glu.h>
#include "Point2d.h"
#include "GameObject.h"
#include "nvImage.h"

class Menu{
protected:
	Point2d* mousePos;
	bool* leftPressed;
	GameObject resumeButton;
	GameObject exitButton;
	GameObject l1Button;
	GameObject l2Button;
	GameObject l3Button;
	bool levelSelect;
	int level;
	bool exit;
	bool active;
	std::vector<char*> texNames;
	std::vector<GLuint> textures;
	GLuint resumeTexture;
	GLuint exitTexture;
	GLuint l1Texture;
	GLuint l2Texture;
	GLuint l3Texture;


public:
	Menu();
	Menu(Point2d* mousePos, bool* leftPressed);
	void draw();
	bool isActive();
	void setActive();
	bool exitGame();
	bool levelSelected();
	void setLevelSelectedFalse();
	int getLevel();
	void checkButtonPress();
	void loadTextures();
	GLuint loadPNG(char* name);
};