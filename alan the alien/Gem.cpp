#include "Gem.h"
//constructor
Gem::Gem(Point2d c){
	centre = c;
	width = 2;
	height = 2;
	rad = sqrt(8.0f);
	texName = "Items/gemBlue.png";
	pickedUp = false;
}
//draw method
void Gem::draw(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, texture);//set texture
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw gem
	glBegin(GL_POLYGON);
		glTexCoord2f(0.25, 0.75);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.25, 0.25);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(0.75, 0.25);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(0.75, 0.75);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();
}
//get pickedup status
bool Gem::isPickedUp(){return pickedUp;}
//method to pick up gem
void Gem::pickUp(){pickedUp = true;}
//method to load texture
void Gem::loadTexture(){
	texture = loadPNG(texName);
}