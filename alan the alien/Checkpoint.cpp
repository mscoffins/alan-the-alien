#include "Checkpoint.h"
//constructor for checkpoint object
Checkpoint::Checkpoint(Point2d c){
	width = 3.5;
	height = 4;
	centre = c;
	texNames.push_back("Items/flagGreenHanging.png");
	texNames.push_back("Items/flagGreen.png");
	texNames.push_back("Items/flagGreen2.png");
	count = 0;
	active = false;
}
//draw method for checkpoint object
void Checkpoint::draw(){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	//bind texture of flag and set it to CLAMP_TO_EDGE
	glBindTexture(GL_TEXTURE_2D, currentTex);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//enable blending for textures alpha values
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw checkpoint object
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 1);glVertex2f(centre.getX() - width/2 ,centre.getY() + height/2);
		glTexCoord2f(0.0, 0.0);glVertex2f(centre.getX() - width/2 ,centre.getY() - height/2);
		glTexCoord2f(1, 0.0);glVertex2f(centre.getX() + width/2 ,centre.getY() - height/2);
		glTexCoord2f(1, 1);glVertex2f(centre.getX() + width/2 ,centre.getY() + height/2);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();
};
//update method for checkpoint
void Checkpoint::update(double deltaT){
	//if the checpoint is active update the texture according to the count variable
	if(active){
		count += deltaT*2;
		if(count > 1)
			currentTex = textures.at(2);
		else
			currentTex = textures.at(1);

		if(count > 2)
			count = 0;
	}
}
//get active status
bool Checkpoint::isActive(){return active;}
//activate checkpoint
void Checkpoint::setActive(){active = true;}
//load checkpoint textures
void Checkpoint::loadTextures(){
	for each(char* tex in texNames){
		textures.push_back(loadPNG(tex));
	}
	currentTex = textures.at(0);
}

